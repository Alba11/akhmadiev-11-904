
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Array;
import java.time.LocalDate;

public class PaymentServiceTest {
    Payment[] payments;

    private Payment paymentNull = null;
    private Payment paymentYesterday1 = new Payment(LocalDate.now().minusDays(1), 1, "smallYesterday");
    private Payment paymentYesterday2 = new Payment(LocalDate.now().minusDays(1), 2, "bigYesterday");
    private Payment paymentYesterday3 = new Payment(LocalDate.now().minusDays(1), 2, "bigYesterday");
    private Payment paymentTomorrow1 = new Payment(LocalDate.now().plusDays(1), 1, "smallTomorrow");
    private Payment paymentTomorrow2 = new Payment(LocalDate.now().plusDays(1), 2, "bigTomorrow");

    @Test
    public void testFindNullPointerArr(){
        Payment[] payments = null;
        Assert.assertThrows(NullPointerException.class, ()->PaymentService.find(payments));
    }

    @Test
    public void testFindNullPointerElem(){
        Payment[] payments = {paymentNull, paymentTomorrow1};
        Assert.assertThrows(NullPointerException.class, ()->PaymentService.find(payments));
    }

    @Test
    public void testFindSamePayments(){
        Payment[] payments = {paymentYesterday1, paymentYesterday2, paymentYesterday3};
        String actual = PaymentService.find(payments);
        String expected = "bigYesterday";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFindSamePaymentsTomorrow(){
        Payment[] payments = {paymentTomorrow2, paymentTomorrow1};
        String actual = PaymentService.find(payments);
        String expected = "bigTomorrow";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFindSamePaymentsTomorrowAndYesterday(){
        Payment[] payments = {paymentYesterday1, paymentYesterday2, paymentTomorrow1, paymentTomorrow2, paymentYesterday3};
        String actual = PaymentService.find(payments);
        String expected = "bigTomorrow";
        Assert.assertEquals(expected, actual);
    }


}
