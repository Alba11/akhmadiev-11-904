import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
public class Payment {
    private LocalDate paymentDate;
    private long amount;
    private String name;
}
