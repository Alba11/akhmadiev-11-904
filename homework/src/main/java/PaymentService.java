import java.time.LocalDateTime;

public class PaymentService {
    public static String find(Payment[] payments) throws NullPointerException{
        if (payments == null || payments[0] == null) throw new NullPointerException();
        Payment min = payments[0];
        for(int i = 1; i < payments.length; i ++){
            if(min.getPaymentDate().isBefore(payments[i].getPaymentDate())){
                min = payments[i];
            }else if(min.getPaymentDate().isEqual(payments[i].getPaymentDate())){
                if(min.getAmount() < payments[i].getAmount()){
                    min = payments[i];
                }
            }
        }
        return min.getName();
    }
}