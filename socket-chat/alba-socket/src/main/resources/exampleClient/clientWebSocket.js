var stompClient;
let currentUserId = 0;
let  recipientId = 0;

function send() {
    let newMessage = document.getElementById("message")
        .value
    sendMessage(newMessage)
    document.getElementById("message").value = '';
}

const sendMessage = (msg) => {
    if (msg.trim() !== "") {
        const message = {
            senderId: currentUserId,
            recipientId: recipientId,
            message: msg,
            date: new Date(),
        };

        stompClient.send("/webSocketApp/chat", {}, JSON.stringify(message));
    }
};

function connect() {
    SockJS = new SockJS("http://localhost:8080/ws");
    stompClient = Stomp.over(SockJS);
    currentUserId = document.getElementById("currentUserId").value
    currentUserId = document.getElementById("recipientId").value
    stompClient.connect({}, onConnected, onError);
}

function onError() {
    console.log("error during connection")
}

function exit() {
    document.getElementById("connectionDiv")
        .setAttribute("style", "visibility: visible")
    document.getElementById("chatDiv")
        .setAttribute("style", "visibility: hidden")
    stompClient.terminate({}, onDisconnected(), onError())
}

function onConnected() {
    document.getElementById("connectionDiv")
        .setAttribute("style", "visibility: hidden")
    document.getElementById("chatDiv")
        .setAttribute("style", "visibility: visible")
    document.getElementById("welcome").innerText = "Welcome, " + currentUserId + " user"

    stompClient.subscribe(
        "/user/" + recipientId + "/queue/messages",
        received
    );
}

function onDisconnected() {
    document.getElementById("connectionDiv")
        .setAttribute("style", "visibility: visible")
    document.getElementById("chatDiv")
        .setAttribute("style", "visibility: hidden")

    stompClient.stop()
}

function received(message) {
    let liFirst = document.createElement('li');
    let date = new Date(Date.parse(message.body).date)
    liFirst.innerHTML = "(" + date.getHours() + ") " + JSON.parse(message.body).senderId + ": " + JSON.parse(message.body).message;
    let ol = document.getElementById("ol");
    ol.before(liFirst);
}
