package ru.itis.albasocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlbaSocketApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlbaSocketApplication.class, args);
    }

}
