package ru.itis.albasocket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.albasocket.model.ChatRoom;
import ru.itis.albasocket.repository.ChatRoomRepository;

@Service
public class ChatService {

    @Autowired
    private ChatRoomRepository chatRoomRepository;

    public ChatRoom getOrCreate(String senderId, String recipientId) {
        ChatRoom chatRoom = chatRoomRepository.findByUsers(senderId, recipientId);

        if(chatRoom == null) {
            ChatRoom newChatRoom = new ChatRoom();
            newChatRoom.setUser1Id(senderId);
            newChatRoom.setUser2Id(recipientId);

            return chatRoomRepository.save(newChatRoom);
        }
        return chatRoom;
    }
}
