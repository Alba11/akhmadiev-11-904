package ru.itis.albasocket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.albasocket.model.ChatMessage;

public interface ChatMessageRepository extends JpaRepository<ChatMessage, Integer> {

}
