package ru.itis.albasocket.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "itis_messages")
@Data
public class ChatMessage {

    @Id
    @GeneratedValue
    private int id;

    private String message;

    private String senderId;

    private String recipientId;

    @ManyToOne(fetch = FetchType.EAGER)
    private ChatRoom chatRoom;

    private Date date;
}
