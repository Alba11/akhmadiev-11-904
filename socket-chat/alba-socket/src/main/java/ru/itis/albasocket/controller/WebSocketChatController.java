package ru.itis.albasocket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import ru.itis.albasocket.model.ChatMessage;
import ru.itis.albasocket.model.ChatRoom;
import ru.itis.albasocket.service.ChatMessageService;
import ru.itis.albasocket.service.ChatService;

@Controller
public class WebSocketChatController {
    @Autowired
    private ChatService chatService;

    @Autowired
    private ChatMessageService messageService;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/chat")
    public void chat(@Payload ChatMessage message) {
        ChatRoom chatRoom = chatService.getOrCreate(message.getSenderId(), message.getRecipientId());
        message.setChatRoom(chatRoom);
        message = messageService.save(message);

        messagingTemplate.convertAndSendToUser(message.getRecipientId(), "/queue/messages", message);
    }
}
