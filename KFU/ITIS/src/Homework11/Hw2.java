package Homework11;

import java.util.Scanner;

public class Hw2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] a = new int[n][n];

        a[0][0] = 1;
        System.out.print(a[0][0] + " ");
        System.out.println();


        for (int i = 1; i < n; i++) {
            a[0][0] = 1;
            System.out.print(a[0][0] + " ");
            for (int j = 1; j < n; j++) {
                a[i][j] = a[i - 1][j - 1] + a[i - 1][j];
                a[i][0] = 1;
                if (a[i][j] != 0) {
                    System.out.print(a[i][j] + " ");
                }
            }
            System.out.println();
        }
    }
}
