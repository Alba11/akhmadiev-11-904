package Homework11;

import java.util.Random;
import java.util.Scanner;

public class HW1_Friday {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        Random r = new Random();
        int max1 = 0;
        int max2 = 0;
        int sum = 0;

        int s = 1;
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = r.nextInt(n);
            System.out.print(a[i] + " ");

            if (a[i] > max1 && i % 2 != 0) {
                max1 = a[i];
                s = 1;
            } else if (a[i] == max1 && i % 2 != 0) {
                s++;
            }
            if (a[i] > max2 && i % 2 == 0) {
                max2 = a[i];
                s = 0;
            }
            if (i % 2 != 0) {
                sum += a[i];
            }
        }
        System.out.println();
        int max;
        if (max1 >= max2) {
            max = max1;
        } else {
            max = 0;
        }
        System.out.println(sum - s * max);
    }
}
