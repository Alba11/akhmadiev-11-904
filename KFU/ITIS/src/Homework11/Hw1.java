package Homework11;

import java.util.Random;
import java.util.Scanner;

public class Hw1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();

        int n = sc.nextInt();
        int m = sc.nextInt();
        int[][] a = new int[n][m + 1];
        int[] n1 = new int[n];

        int sum = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = r.nextInt(n);
                System.out.print(a[i][j] + " ");
                sum += a[i][j];
            }
            System.out.println();
            a[i][m] = sum;
            sum = 0;
        }
        System.out.println();

        for(int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                if (a[j][m] > a[j + 1][m]) {
                    n1 = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = n1;
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m + 1; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}

