package Homework11;

import java.util.Scanner;

public class Hw3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] a = new int[n+1][n+1];

        a[0][0] = 1;

        for(int i = 0; i < n+1; i++){
            for(int j = n+1-i; j > 0; j--){
                System.out.print(" ");
            }
            if(i != 0) {
                for (int j = 1; j < n+1; j++) {
                    a[i][j] = a[i-1][j-1] + a[i-1][j];
                    if(a[i][j] % 2 == 0 && a[i][j] != 0) {
                        System.out.print("*" + " ");
                    }else if(a[i][j] % 2 != 0 && a[i][j] != 0) {
                        System.out.print("0" + " ");
                    }
                }
            }else{
                System.out.print(" ");
            }
            System.out.println();
        }
    }
}
