package Homework12;

import java.util.Random;
import java.util.Scanner;

public class Hw1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();

        int n = sc.nextInt();
        sc.nextLine();

        String[] string = new String[n];

        for (int i = 0; i < n; i++) {
            string[i] = sc.nextLine();
        }
        String k = "";
        int d = 0;
        for (int j = 0; j < n - 1; j++) {
            for (int i = 0; i < n - 1; i++) {
                d = Hw12.compareTo(string[i], string[i + 1]);
                if ((d == -1) || (d == 0 && string[i].length() > string[i + 1].length())) {
                    k = string[i];
                    string[i] = string[i + 1];
                    string[i + 1] = k;
                }
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.println(i + 1 + ": " + string[i]);
        }
    }

}
