package Homework12;

public class Hw12 {
    public static int compareTo(String s1, String s2) {

        String[] s = new String[2];
        int n = Math.min(s1.length(), s2.length());

        for (int i = 0; i < n; n++) {
            if(s1.charAt(i) < s2.charAt(i)){
                return 1;
            }else if(s1.charAt(i) > s2.charAt(i)) {
                return  -1;
            }
        }
        return 0;
    }
}