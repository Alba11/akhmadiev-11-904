package Homeworks.Homeworks19;

public class YourHouse extends House implements HousePrice {

    @Override
    public double housePrice(int a, int b) {
        Window w = new Window();
        Door d = new Door();
        price = materialOfRoofPrice(b) + materialOfWallPrice(a) + w.windowPrice() + d.doorPrice() + priceOfGarage + priceOfPool;
        return price;
    }

    @Override
    public double materialOfWallPrice(int i) {
        priceMaterialOfWall = (length + wigth) * 2 * height * materialOfWall(i);
        return priceMaterialOfWall;
    }

    @Override
    public double materialOfRoofPrice(int i) {
        priceMaterialOfRoof = square * materialOfRoof(i);
        return priceMaterialOfRoof;
    }

    public class Window implements WindowPrice {

        private int countOdWindow = getCountOfWindow();

        @Override
        public double windowPrice() {
            return countOdWindow * priceOfOneWindow;
        }

    }

    public class Door implements DoorPrice {
        private int countOfDoor = getCountOfDoor();

        @Override
        public double doorPrice() {
            return countOfDoor * priceOfOneDoor;
        }
    }

    public static class PlaceOfConsyruction {
        String place = nameOfThePlaceOfConstruction;

        public void printThePlace() {
            System.out.println("Город в котором вы хотите построить дом: " + place);
        }

        public String getPlace() {
            return place;
        }
    }
}
