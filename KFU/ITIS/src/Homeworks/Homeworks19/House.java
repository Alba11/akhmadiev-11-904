package Homeworks.Homeworks19;

public abstract class House {
    protected static String nameOfThePlaceOfConstruction = "Alba City";
    protected double priceMaterialOfWall;
    protected double priceMaterialOfRoof;
    protected double price;
    protected int countOfDoor;
    protected int countOfWindow;
    protected double priceOfOneWindow = 9000;
    protected double priceOfOneDoor = 4000;
    protected double wigth;
    protected double length;
    protected double square = wigth * length;
    protected double height;

    protected enum WallMaterial {
        BRICK(500), WOOD(1500), CONCRETE(3000)/*это бетон*/, ROCK(2000), PANEL(7000);
        private double price = 0;

        WallMaterial(double price) {
            this.price = price;
        }

        public double getPrice() {
            return price;
        }

    }

    public double materialOfWall(int i) {
        String name = "";
        double price = 0;
        switch (i) {
            case 1:
                name = "BRICK";
                break;
            case 2:
                name = "WOOD";
                break;
            case 3:
                name = "CONCRETE";
                break;
            case 4:
                name = "ROCK";
                break;
            case 5:
                name = "PANEL";
                break;
        }
        WallMaterial m = WallMaterial.valueOf(name);
        price = m.getPrice();
        return price;
    }

    private enum RoofMaterial {
        SLATE(200), ROOF_TILE(1150);
        private double price;

        RoofMaterial(double price) {
            this.price = price;
        }

        public double getPrice() {
            return price;
        }
    }

    public double materialOfRoof(int i) {
        String name = "";
        double price = 0;
        switch (i) {
            case 1:
                name = "SLATE";
                break;
            case 2:
                name = "ROOF_TILE";
                break;
        }
        RoofMaterial r = RoofMaterial.valueOf(name);
        price = r.getPrice();
        return price;
    }

    protected boolean pool;
    protected double priceOfPool = 0;

    protected boolean garage;
    protected double priceOfGarage = 0;


    public void setPool(boolean pool) {
        this.pool = pool;
        if (pool) {
            priceOfPool = 100000;
        }
    }


    public void setGarage(boolean garage) {
        this.garage = garage;
        if (garage) {
            priceOfGarage = 400000;
        }
    }

    public void setWigth(double wigth) {
        this.wigth = wigth;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setCountOfDoor(int countOfDoor) {
        this.countOfDoor = countOfDoor;
    }

    public void setCountOfWindow(int countOfWindow) {
        this.countOfWindow = countOfWindow;
    }


    public int getCountOfDoor() {
        return countOfDoor;
    }

    public int getCountOfWindow() {
        return countOfWindow;
    }

}
