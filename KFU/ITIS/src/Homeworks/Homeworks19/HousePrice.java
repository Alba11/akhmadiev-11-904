package Homeworks.Homeworks19;

public interface HousePrice {
    public double housePrice(int a, int b);

    public double materialOfWallPrice(int i);

    public double materialOfRoofPrice(int i);
}
