package Homeworks.homework21;

import java.util.Random;

public class Player {
    protected Random r = new Random();
    protected String name;
    protected int hp;
    protected int hit = 0;
    protected int healHp;

    public Player(String name){
        this.name = name;
        hp = 25;
        System.out.println("Player created! Your Name: " + name);
    }

    public int hit(int hit){

        double luck = r.nextDouble();
        double x = hit/10.0;
        if(luck > x){
            this.hit = hit;
            System.out.println("Yes! I got it ^-^");
            return this.hit;
        }
        System.out.println("Ouch, I missed :(");
        return 0;

    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", hp=" + hp +
                '}';
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public int getHit() {
        return hit;
    }

    public int getHealHp() {
        return healHp;
    }
}
