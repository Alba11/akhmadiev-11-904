package Homeworks.homework21;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Welcome to <AlbaGame>");
        System.out.println("Player 1, please enter a name of person:");
        String name1 = sc.nextLine();
        System.out.println("Okay, " + name1 + ", please choose a person class: \n 1) Knight(defoult character who only hits); \n 2) Magican (a character class that can also heal itself)");
        int choose1 = sc.nextInt();
        sc.nextLine();

        System.out.println("Player 2, please enter a name of person:");
        String name2 = sc.nextLine();
        System.out.println("Okay, " + name2 + ", please choose a person class: \n 1) Knight(defoult character who only hits); \n 2) Magican (a character class that can also heal itself)");
        int choose2 = sc.nextInt();

        Game game = new Game(name1, name2, choose1, choose2);

        while (game.isGameEnd() != true){
            game.askHitPlayer1();
            int hit1 = sc.nextInt();
            game.processGameFor1(hit1);
            if(game.isGameEnd() == true){
                break;
            }
            game.askHitPlayer2();
            int hit2 = sc.nextInt();
            game.processGameFor2(hit2);
        }
        game.resultGame();
    }
}
