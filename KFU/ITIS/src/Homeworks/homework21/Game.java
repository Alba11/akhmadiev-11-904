package Homeworks.homework21;

import javax.swing.*;

public class Game {

    private boolean gameEnd;
    private Player player1;
    private Player player2;

    public Game(String name1, String name2, int choose1, int choose2){

        gameEnd = false;

        if(choose1 == 1){
            player1 = new Player(name1);
        }else {
            player1 = new HealerPLayer(name1);
        }

        if(choose2 == 1){
            player2 = new Player(name2);
        }else if (choose2 == 2){
            player2 = new HealerPLayer(name2);
        }

        System.out.println("Game started! 3... 2... 1... FIGHT!");
        System.out.println(player1.getName() + " VS " + player2.getName());

    }

    public void processGameFor1(int hit){
        System.out.println(player1.getName() + " hit " + player2.getName());
        player2.setHp(player2.getHp() - player1.hit(hit));
        System.out.println(player1.toString());
        System.out.println(player2.toString());
        if(player2.getHp() <= 0 || player1.getHp() <= 0) setGameEnd(true);
    }
    public void processGameFor2(int hit){
        System.out.println(player2.getName() + " hit " + player1.getName());
        player1.setHp(player1.getHp() - player2.hit(hit));
        System.out.println(player1.toString());
        System.out.println(player2.toString());
        if(player2.getHp() <= 0 || player1.getHp() <= 0) setGameEnd(true);
    }

    public void askHitPlayer1(){
        if(player1 instanceof HealerPLayer) {
            System.out.println(player1.getName() + ": Please choose power of hit from 1 to 9. But if you wanna heal yourself enter 10");
        }else{
            System.out.println(player1.getName() + ": Please choose power of hit from 1 to 9");
        }
    }
    public void askHitPlayer2(){
        if(player1 instanceof HealerPLayer) {
            System.out.println(player2.getName() + ": Please choose power of hit from 1 to 9. But if you wanna heal yourself enter 10");
        }else{
            System.out.println(player2.getName() + ": Please choose power of hit from 1 to 9");
        }
    }

    public void resultGame(){
        System.out.println("In our game winner is .......");
        String winner;
        if(player1.getHp() <= 0 ){
            winner  = player2.getName() + " " + player2.toString();
        }else{
            winner = player1.getName() + " " + player1.toString();
        }
        System.out.println(winner);
    }

    public boolean isGameEnd() {
        return gameEnd;
    }

    public void setGameEnd(boolean gameEnd) {
        this.gameEnd = gameEnd;
    }
}
