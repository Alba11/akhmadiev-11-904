package Homeworks.test4;

public class Task4Main {
    public static void main(String[] args) {
        double factorial = 1.0;
        double p = 1;
        double x1 = 0;
        double x2 = p/factorial;
        double sum = 0;
        double delta = x2 - x1;
        double k = 1;
        while (delta > 0.000001){
            x1 = x2;
            sum += x1;
            k += 1;
            factorial *= k;
            p *= -1;
            x2 = p/factorial;
            delta = x2 - x1;
            if(delta < 0){
                delta *= -1;
            }
        }
        System.out.println(sum);
    }
}
