package Homeworks.test4;

import java.util.Scanner;

public class Task1Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Введите значение X ваших точек(все 3 значения)");
        int x1 = sc.nextInt();
        int x2 = sc.nextInt();
        int x3 = sc.nextInt();
        int[] aX = {x1, x2, x3};

        System.out.println("Введите значение Y ваших точек(все 3 значения)");
        int y1 = sc.nextInt();
        int y2 = sc.nextInt();
        int y3 = sc.nextInt();
        int[] aY = {y1, y2, y3};

        for (int i = 0; i < 3; i++) {

            if (aX[i] > 0 && aY[i] > 0) {
                System.out.println("Точка " + (i + 1) + " находится в 1ой четверти");
            } else if (aX[i] < 0 && aY[i] > 0) {
                System.out.println("Точка " + (i + 1) + " находится во 2ой четверти");
            } else if (aX[i] < 0 && aY[i] < 0) {
                System.out.println("Точка " + (i + 1) + " находится в 3ей четверти");
            } else if (aX[i] > 0 && aY[i] < 0) {
                System.out.println("Точка " + (i + 1) + " находится в 4ой четверти");
            } else if (aX[i] == 0) {
                System.out.println("Точка " + (i + 1) + " находится на оси ординат");
            } else if (aY[i] == 0) {
                System.out.println("Точка " + (i + 1) + " находится на оси абцисс");
            }else {
                System.out.println("Точка " + (i + 1) + " находится на точке пересечения ординат");
            }

        }
    }
}
