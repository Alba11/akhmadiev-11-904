package Homeworks.test4;

import java.util.Scanner;

public class Task3Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Напишите число, больше 1: ");
        int n = sc.nextInt();

        if (n < 1) {
            System.out.println("Введите корректное число");
            return;
        }
        int sum = 0;
        for (int i = 1; i < n; i++) {
            if (n % i == 0) {
                sum += i;
            }
        }
        if (sum == n) {
            System.out.println("Да " + sum + " = " + n);
        } else {
            System.out.println("Нет " + sum + " != " + n);;
        }
    }
}
