package Homeworks.test4;

import java.util.Scanner;

public class Task2Main {
    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);

        System.out.print("Напишите нечетное число : ");
        int n = sc.nextInt();

        if (n % 2 == 0) {
            System.out.println("Введено некорректное число");
            return;
        }

        for(int i = 0; i < n/2; i++){
            for(int j = i; j < n/2; j++){
                System.out.print("*");
            }
            for(int j = i*2+1; j > 0; j--){
                System.out.print(" ");
            }
            for(int j = i; j < n/2; j++){
                System.out.print("*");
            }
            System.out.println();
        }
        for(int i = 0; i < n/2; i++){
            for(int j = i+1; j > 0; j--){
                System.out.print("*");
            }
            for(int j = i*2+1; j <= n - 2; j++){
                System.out.print(" ");
            }
            for(int j = i+1; j > 0; j--){
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
