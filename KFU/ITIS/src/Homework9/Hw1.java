package Homework9;

import java.util.Random;
import java.util.Scanner;

public class Hw1 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        Random r = new Random();
        int max = 0;
        int sum = 0;

        int s = 1;
        int[] a = new int[n];
        int[] b = new int[n/2];
        for (int i = 0; i < n; i++) {
            a[i] = r.nextInt(n);
            System.out.print(a[i] + " ");

            if(a[i] > max && i % 2 != 0) {
                max = a[i];
                s = 1;
            }else if(a[i] == max && i % 2 != 0) {
                s++;
            }else if(a[i] > max && i % 2 == 0){
                max = 0;
            }
            if(i % 2 != 0) {
                sum += a[i];
            }
        }
        System.out.println();

        System.out.println(sum - s*max);
    }
}
