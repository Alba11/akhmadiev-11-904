package Homework9;

import java.util.Random;
import java.util.Scanner;

public class Hw2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        Random r = new Random();
        int min;
        int s = 0;

        int[] a = new int[n];

        for (int i = 0; i < n; i++) {
            a[i] = r.nextInt(n);
            System.out.print(a[i] + " ");
        }
        System.out.println();
        min = a[0];
        for(int i = 1; i < n; i++){
            if(a[i] <= min){
                min = a[i];
            }
        }
        for(int i = 0; i < n; i ++){
            if(a[i] == min){
                s++;
            }
        }
        System.out.println(s);
    }
}
