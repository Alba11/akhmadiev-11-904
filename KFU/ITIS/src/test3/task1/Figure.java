package test3.task1;

public abstract class Figure {
    protected double square;
    protected double perimeter;
    public abstract double countSquare();

    public abstract double countPerimeter();

    public double getSquare() {
        return square;
    }

    public double getPerimeter() {
        return perimeter;
    }
}
