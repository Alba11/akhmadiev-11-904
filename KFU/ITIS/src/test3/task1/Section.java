package test3.task1;

public class Section {
    private final Point point1;
    private final Point point2;
    private final double size;

    public Section(int x1, int y1, int x2, int y2){
        point1 = new Point(x1, y1);
        point2 = new Point(x2, y2);
        size = sizeSection();
    }

    private double sizeSection(){
        return Math.sqrt((point2.getX()-point1.getX())*(point2.getX()-point1.getX())+(point2.getY()-point1.getY())*(point2.getY()-point1.getY()));
    }

    public class Point{
        private final int x;
        private final int y;
        public Point(int x, int y){
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }

    public double getSize() {
        return size;
    }
}
