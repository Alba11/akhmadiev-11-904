package test3.task1;

public class Main {
    public static void main(String[] args) {
        int x1 = 0;
        int y1 = 0;

        int x2 = 4;
        int y2 = 0;

        int x3 = 4;
        int y3 = 3;

        int x4 = 0;
        int y4 = 3;

        Section section1 = new Section(x1, y1, x2, y2);
        Section section2 = new Section(x2, y2, x3, y3);
        Section section3 = new Section(x3, y3, x4, y4);
        Section section4 = new Section(x4, y4, x1, y1);

        Rectangle rectangle = new Rectangle(section1, section2, section3, section4);
    }
}
