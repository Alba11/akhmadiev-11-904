package test3.task1;

public class Rectangle extends Figure implements Printable{
    private Section a; //это длина
    private Section b;//это ширина
    private Section c;
    private Section d;
    private Color color;


    public Rectangle(Section a, Section b, Section c, Section d){
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        square = countSquare();
        perimeter = countPerimeter();
        color = setColor();
        print();
    }

    public Color setColor(){
        if(perimeter > 100){
            color = Color.RED;
        }else if(perimeter > 20){
            color = Color.YELLOW;
        }else {
            color = Color.GREEN;
        }
        return color;
    }

    @Override
    public void print() {
        System.out.println("Figure: Rectangle");
        System.out.println("Perimeter Rectangle = " + perimeter);
        System.out.println("Square Rectangle = " + square);
        System.out.println("Color figure: " + color);
    }

    @Override
    public double countSquare() {
        return a.getSize()*b.getSize();
    }

    @Override
    public double countPerimeter() {
        return (a.getSize()+b.getSize())*2;
    }
}
