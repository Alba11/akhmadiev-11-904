package Homework18;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Как вы хотите ввести чилсо? Если сами то нажмите 1, если случайно нажмите 0");
        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        int f = sc.nextInt();

        int num = 0;

        if(f == 1) {
            num = sc.nextInt();
        }else if(f == 0){
            num = r.nextInt(1000000) + 10000000;
            System.out.println(num);
        }else {
            System.out.println("Вы должны были ввести 1 или 0. Перезапустите программу и введите коректное число");
        }

        MyNum value = new MyNum(num);
        // передаю именно через конструктор, а тот setValue я создал потому что вы попросили,  не совсем понял зачем создаватать и метод, и конструтор которые делают одно и то же
        int ans = value.getSum();
        value.systemOut(ans);

    }
}
