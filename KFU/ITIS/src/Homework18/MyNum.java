package Homework18;

import java.util.Random;
import java.util.Scanner;

public class MyNum {

    Scanner sc = new Scanner(System.in);
    Random r = new Random();

    private int value;
    private int s;


    protected MyNum(int value) {
        this.value = value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getSum() {
        return sum(this.value);
    }

    private int sum(int value) {
        this.value = value;
        if (this.value > 0) {
            s += this.value % 10;
            sum(this.value / 10);
        }
        return s;
    }

    public void systemOut(int ans) {
        System.out.println("Ответ: " + ans);
    }
}
