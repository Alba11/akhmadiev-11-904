package Homework17.Animals;

import Homework17.Animal;
import Homework17.Pet;

public class Dog extends Animal implements Pet {
    private String name = getName();

    public Dog(String name) {
        super(name);
    }

    private double satietyLevel = getSatietyLevel(), /*Уровень голода*/
            sleepLevel = getSleepLevel(), happyLevel = getHappyLevel();

    @Override
    public void play(int game) {
        if (game == 1) {
            happyLevel += 20;
            satietyLevel -= 25;
            sleepLevel -= 30;
        } else {
            happyLevel += 25;
            satietyLevel -= 30;
            sleepLevel -= 35;
        }
        System.out.println(name + ": Мне очень весело с тобой играть!");
        printAll();
        System.out.println();
    }

    @Override
    public void voice() {
        System.out.println(name + ": ГАВ-ГАВ^^ Спасибо тебе за все) и как не стрнанно это все! <3");
        System.out.println();
    }

    public void dog(int f) {
        if (f == 1) {
            happyLevel += 20;
            System.out.println(name + ": ГАВ-ГАВ");
        } else {
            happyLevel -= 20;
            System.out.println(name + ": ОТ ТЕБЯ ТАКОГО Я НЕ ОЖИДАЛ");
        }
        printAll();
        System.out.println();
    }
}


