package Homework17;

import Homework17.Animals.*;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Выберете цифру животоного за которым хотите ухаживать?");
        String animal1 = "Кошечка ";
        String animal2 = "Собачка ";
        String animal3 = "Тигренок ";
        String animal4 = "Слоненок ";
        String animal5 = "Носорожек ";
        System.out.println("1)" + animal1 + "2)" + animal2 + "3)" + animal3 + "4)"+ animal4 + "5)" + animal5);
        int animal = sc.nextInt();
        sc.nextLine();
        System.out.println("Какое имя вы дадите своему питомцу?");
        String name = sc.nextLine();

        if(animal == 1){

            Cat cat = new Cat(name);
            cat.printAll();
            System.out.println(name + ": Я хочу кушать! Покорми меня) " + "Чем вы покормите своего питомца? " + "1) Банан " + "2) Рыбка " + "3) Чизкейк");
            int food = sc.nextInt();
            cat.eat(food);
            System.out.println(name + ": Теперь я хочу играть!!!!! Поиграй со мной. Чем хотите заняться со своим питомцем? 1) Погулять 2) Поиграть в мячик ");
            int game = sc.nextInt();
            cat.play(game);
            System.out.println(name + ": Погладишь меня? 1) Да 2) Нет ");
            int f = sc.nextInt();
            cat.cat(f);
            System.out.println(name + ": Может поспим? 1) Да 2) Нет ");
            int p = sc.nextInt();
            cat.sleep(p);
            cat.voice();

        }else if(animal == 2){

            Dog dog = new Dog(name);
            dog.printAll();
            System.out.println(name + ": Я хочу кушать! Покорми меня) " + "Чем вы покормите своего питомца? " + "1) Косточка " + "2) Стэйк " + "3) Чизкейк");
            int food = sc.nextInt();
            dog.eat(food);
            System.out.println(name + ": Теперь я хочу играть!!!!! Поиграй со мной. Чем хотите заняться со своим питомцем? 1) Погулять 2) Поиграть в мячик ");
            int game = sc.nextInt();
            dog.play(game);
            System.out.println(name + ": Кинешь мне палку? А я тебе её принесу) 1) Да 2) Нет ");
            int f = sc.nextInt();
            dog.dog(f);
            System.out.println(name + ": Может поспим? 1) Да 2) Нет ");
            int p = sc.nextInt();
            dog.sleep(p);
            dog.voice();

        }else if(animal == 3){
            Tiger tiger = new Tiger(name);

            tiger.printAll();
            System.out.println(name + ": Я хочу кушать! Покорми меня) " + "Чем вы покормите своего питомца? " + "1) Курица " + "2) Рыба " + "3) Говядина");
            int food = sc.nextInt();
            tiger.eat(food);
            System.out.println(name + ": Теперь я хочу играть!!!!! Поиграй со мной. Чем хотите заняться со своим питомцем? 1) Погулять 2) Побегать на спор - кто быстрее? ");
            int game = sc.nextInt();
            tiger.play(game);
            System.out.println(name + ": Можешь причесать меня расчесткой?) Ты ведь знаешь как мне это нравится) 1) Да 2) Нет ");
            int f = sc.nextInt();
            tiger.tiger(f);
            System.out.println(name + ": Может поспим? 1) Да 2) Нет ");
            int p = sc.nextInt();
            tiger.sleep(p);
            tiger.voice();

        }else if(animal == 4){
            Elephant elephant = new Elephant(name);

            elephant.printAll();
            System.out.println(name + ": Я хочу кушать! Покорми меня) Слоны очень мого кушают! " + "Чем вы покормите своего питомца? " + "1) Банан " + "2) Яблочко " + "3) Чизкейк");
            int food = sc.nextInt();
            elephant.eat(food);
            System.out.println(name + ": Теперь я хочу играть!!!!! Поиграй со мной. Чем хотите заняться со своим питомцем? 1) Погулять на слоне 2) Обнять Слоника  ");
            int game = sc.nextInt();
            elephant.play(game);
            System.out.println(name + ": Можешь почесать меня за ушком?) Только смотри, оно у меня очень большое) 1) Да 2) Нет ");
            int f = sc.nextInt();
            elephant.elephant(f);
            System.out.println(name + ": Может поспим? 1) Да 2) Нет ");
            int p = sc.nextInt();
            elephant.sleep(p);
            elephant.voice();

        }else if(animal == 5){
            Rino rino = new Rino(name);

            rino.printAll();
            System.out.println(name + ": Я хочу кушать! Покорми меня) Носороги очень большие и часто хоят кушать " + "Чем вы покормите своего питомца? " + "1) Банан " + "2) Трава " + "3) Трава высшего сорта");
            int food = sc.nextInt();
            rino.eat(food);
            System.out.println(name + ": Теперь я хочу играть!!!!! Поиграй со мной. Чем хотите заняться со своим питомцем? 1) Пробить стенку носом 2) Погулять ");
            int game = sc.nextInt();
            rino.play(game);
            System.out.println(name + ": Можешь почесать мой носик?) Тольоко осорожней! Он остренький) 1) Да 2) Нет ");
            int f = sc.nextInt();
            rino.rino(f);
            System.out.println(name + ": Может поспим? 1) Да 2) Нет ");
            int p = sc.nextInt();
            rino.sleep(p);
            rino.voice();
        }


    }
}
