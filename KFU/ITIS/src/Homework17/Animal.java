package Homework17;

public abstract class Animal {

    private String name;
    private int age;
    private double satietyLevel, /*Уровень голода*/
            sleepLevel, happyLevel;

    protected Animal(String name) {
        this.happyLevel = 40;
        this.satietyLevel = 35;
        this.sleepLevel = 75;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void printAll(){
        System.out.println("1) Уровень счастья = " + this.happyLevel);
        System.out.println("2) Урвень сытости = " + this.satietyLevel);
        System.out.println("3) Уровень бодрости = " + this.sleepLevel);
}

    public void setName(String name) {
        this.name = name;
    }

    public double getSatietyLevel() {
        return satietyLevel;
    }

    public void setSatietyLevel(double satietyLevel) {
        this.satietyLevel = satietyLevel;
    }

    public double getSleepLevel() {
        return sleepLevel;
    }

    public void setSleepLevel(double sleepLevel) {
        this.sleepLevel = sleepLevel;
    }

    public double getHappyLevel() {
        return happyLevel;
    }

    public void setHappyLevel(double happyLevel) {
        this.happyLevel = happyLevel;
    }

    public void sleep(int p) {
        if(p == 1) {
            sleepLevel = 100;
            if (satietyLevel >= 50) {
                happyLevel += 25;
            } else {
                happyLevel -= 15;
            }
            satietyLevel -= 30;
            System.out.println(name + ": Спасибо Хозяюшка) Мне приснился очень приятный сон! Мне снилась ты!!!!!");
        }else{
            System.out.println(name + ": Ууууу. И как я смогу играть с тобой, если я хочу спать!");
        }
        printAll();
        System.out.println();
    }

    public void eat(int i){
        if(i == 1){
            satietyLevel += 10;
            happyLevel += 25;
        }else if(i == 2){
            satietyLevel += 20;
            happyLevel += 20;
        } else if(i == 3){
            satietyLevel += 25;
            happyLevel += 10;
        }
        System.out.println(name + ": Спасибоо ^*^ Было очень вкусно!");
        printAll();
        System.out.println();
    }
}
