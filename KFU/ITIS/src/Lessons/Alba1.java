package Lessons;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Alba1 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        Random r = new Random();

        int[] a = new int[n];
        int[] b = new int[m];

        for(int i = 0; i < n; i ++){
            a[i] = r.nextInt(n);
            System.out.print(a[i] + " ");
        }
        System.out.println();

        for(int i = 0; i < m; i ++){
            b[i] = r.nextInt(m);
            System.out.print(b[i] + " ");
        }

        System.out.println();
        int[]  s= Alba2.s(a, b);
        for(int el: s) {
            System.out.print(el+ " ");
        }
    }
}
