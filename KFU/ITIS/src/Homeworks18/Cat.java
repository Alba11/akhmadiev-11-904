package Homeworks18;

import Homeworks.Homework17.Animal;
import Homeworks.Homework17.Pet;

import java.util.Objects;

public class Cat extends Animal implements Pet {
    private String name = getName();

    private double satietyLevel = getSatietyLevel(), /*Уровень голода*/
            sleepLevel = getSleepLevel(), happyLevel = getHappyLevel();

            public Cat(String name){
                super(name);
            }

    @Override
    public void play(int game) {
        if(game == 1) {
            happyLevel = happyLevel+ 20;
            satietyLevel = satietyLevel - 25;
            sleepLevel = sleepLevel- 30;
        }else{
            happyLevel = happyLevel + 25;
            satietyLevel = satietyLevel - 30;
            sleepLevel = sleepLevel - 35;
        }
        setHappyLevel(happyLevel);
        setSleepLevel(sleepLevel);
        setSatietyLevel(satietyLevel);
        System.out.println(name + ": Мне очень весело с тобой играть!");
        printAll();
        System.out.println();
    }

    @Override
    public void voice() {
        System.out.println(name + ": МЯУУУ^^ Спасибо тебе за все) и как не стрнанно это все! <3");
        System.out.println();
    }

    public void cat(int f){
                if(f == 1) {
                    happyLevel = happyLevel + 20;
                    System.out.println(name + ": МЯУУУУ");
                }else {
                    happyLevel = happyLevel - 20;
                    System.out.println(name + ": ОТ ТЕБЯ ТАКОГО Я НЕ ОЖИДАЛА");
                }
                setHappyLevel(happyLevel);
                printAll();
        System.out.println();
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", satietyLevel=" + satietyLevel +
                ", sleepLevel=" + sleepLevel +
                ", happyLevel=" + happyLevel +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cat cat = (Cat) o;
        return Double.compare(cat.satietyLevel, satietyLevel) == 0 &&
                Double.compare(cat.sleepLevel, sleepLevel) == 0 &&
                Double.compare(cat.happyLevel, happyLevel) == 0 &&
                Objects.equals(name, cat.name);
    }

    @Override
    public int hashCode() {
        return (-1)*(17*(int)satietyLevel + 31*(int)happyLevel)*name.hashCode();
            }
}






































