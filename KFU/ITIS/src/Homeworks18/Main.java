package Homeworks18;

import Homeworks.Homework17.Animals.Cat;
import Homeworks.Homework17.Animals.*;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Выберете цифру животоного за которым хотите ухаживать?");
        String animal1 = "Кошечка ";

        System.out.println("1)" + animal1);
        int animal = sc.nextInt();
        sc.nextLine();
        System.out.println("Какое имя вы дадите своему питомцу?");
        String name = sc.nextLine();

        Cat catFirst = new Cat("Alba");

        if(animal == 1){

            Cat cat = new Cat(name);
            cat.printAll();
            System.out.println(name + ": Я хочу кушать! Покорми меня) " + "Чем вы покормите своего питомца? " + "1) Банан " + "2) Рыбка " + "3) Чизкейк");
            int food = sc.nextInt();
            cat.eat(food);
            System.out.println(name + ": Теперь я хочу играть!!!!! Поиграй со мной. Чем хотите заняться со своим питомцем? 1) Погулять 2) Поиграть в мячик ");
            int game = sc.nextInt();
            cat.play(game);
            System.out.println(name + ": Погладишь меня? 1) Да 2) Нет ");
            int f = sc.nextInt();
            cat.cat(f);
            System.out.println(name + ": Может поспим? 1) Да 2) Нет ");
            int p = sc.nextInt();
            cat.sleep(p);
            cat.voice();

            System.out.println("1) HashCode Главного кота: " + catFirst.hashCode());
            System.out.println("2) HashCode Вашего кота: " + cat.hashCode());
            System.out.println("3) Одинаков ли мой и твой кот?) - " + cat.equals(catFirst));
            System.out.println("4) Характеристика Главного кота: " + catFirst.toString());
            System.out.println("5) Характеристика твоего кота: " + cat.toString());

        }


    }
}
