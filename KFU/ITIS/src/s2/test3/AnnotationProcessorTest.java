package KFU.ITIS.src.s2.test3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.nio.file.NoSuchFileException;

import static org.junit.jupiter.api.Assertions.*;

public class AnnotationProcessorTest {
    private AnnotationProcessor processor;

    @BeforeEach
    public void prepare() {
        processor = new AnnotationProcessorImpl();
    }

    @Test
    public void testSuccess_defaultLineTrue() throws Exception {
        var cat = new Cat1();
        cat.setName("Alba");
        var result = processor.process(Cat1.class, cat);
        assertTrue(result);
    }

    @SourceFile(path = "KFU/ITIS/src/s2/test3/input1")
    class Cat1 {
        private String ownerName;
        @CheckValue
        private String name;

        public void setName(String name) {
            this.name = name;
        }
    }

    @Test
    public void testSuccess_secondLineTrue() throws Exception {
        var cat = new Cat2();
        cat.setName("Alba");
        var result = processor.process(Cat2.class, cat);
        assertTrue(result);
    }

    @Test
    public void testSuccess_secondLineFalse() throws Exception {
        var cat = new Cat2();
        cat.setName("cat1");
        var result = processor.process(Cat2.class, cat);
        assertFalse(result);
    }

    @SourceFile(path = "KFU/ITIS/src/s2/test3/input2")
    class Cat2 {
        private String ownerName;
        @CheckValue(line = 2)
        private String name;

        public void setName(String name) {
            this.name = name;
        }
    }

    @Test
    public void testExeption_illegalFieldType() throws Exception {
        var cat = new Cat3();
        assertThrows(InvalidFormatExeption.class, () -> {
            processor.process(Cat3.class, cat);
        });
    }

    @Test
    public void testExeption_illegalPathFormat() throws Exception {
        var cat = new Cat4();
        assertThrows(NoSuchFileException.class, () -> {
            processor.process(Cat4.class, cat);
        });
    }

    @Test
    public void testExeption_NullPointerExeption() throws Exception {
        var cat = new Cat5();
        assertThrows(InvalidFormatExeption.class, () -> {
            processor.process(Cat5.class, cat);
        });
    }

    @Test
    public void testExeption_AnnotationNotFoundExeption() throws Exception {
        var cat = new Cat7();
        assertThrows(AnnotationNotFoundExeption.class, () -> {
            processor.process(Cat7.class, cat);
        });
    }


    @SourceFile(path = "KFU/ITIS/src/s2/test3/input2")
    class Cat3 {
        @CheckValue(line = 2)
        private int age = 1;
        private String ownerName;
        private String name;
    }

    @SourceFile(path = "KFU/ITIS/src/s2/test3/input11")
    class Cat4 {
        @CheckValue(line = 2)
        private int age = 1;
        private String ownerName;
        private String name;
    }

    @SourceFile(path = "KFU/ITIS/src/s2/test3/input2")
    class Cat5 {
        @CheckValue(line = 20)
        private int age = 1;
        private String ownerName;
        private String name;
    }

    class Cat7 {
        private int age = 1;
        private String ownerName;
        private String name;

    }
}