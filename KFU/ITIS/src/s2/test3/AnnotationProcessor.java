package KFU.ITIS.src.s2.test3;

import java.io.IOException;

public interface AnnotationProcessor {
    boolean process(final Class<?> cl,final Object object)
            throws InvalidFormatExeption, AnnotationNotFoundExeption, IOException, IllegalAccessException;
}
