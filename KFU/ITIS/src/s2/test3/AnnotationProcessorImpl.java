package KFU.ITIS.src.s2.test3;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Stream;

public class AnnotationProcessorImpl implements AnnotationProcessor {
    @Override
    public boolean process(final Class<?> cl, final Object object) throws AnnotationNotFoundExeption, InvalidFormatExeption, IOException, IllegalAccessException {
        if (cl.isAnnotationPresent(SourceFile.class)) {
            String fileName = cl.getAnnotation(SourceFile.class).path();

            String[] strings = Files.lines(Paths.get(fileName)).toArray(String[]::new);

            Field[] fields = Arrays.stream(cl.getDeclaredFields()).filter(x -> x.isAnnotationPresent(CheckValue.class)).toArray(Field[]::new);

            for(Field f : fields ){
                f.setAccessible(true);
                if(f.getType().equals(String.class)){
                    return f.get(object).equals(strings[f.getAnnotation(CheckValue.class).line()-1]);
                }else {
                    throw new InvalidFormatExeption();
                }
            }
            return false;
        }else{
            throw new AnnotationNotFoundExeption();
        }
    }
}
