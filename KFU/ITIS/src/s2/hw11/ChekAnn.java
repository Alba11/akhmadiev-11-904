package KFU.ITIS.src.s2.hw11;

import java.lang.reflect.Field;

class ChekAnn {
    static void positive(Field field, Object o, int x) throws IllegalAccessException, InvalidDataExeption {
        if(field.isAnnotationPresent(Positive.class)){
            if(x > 0){
                field.set(o, x);
            }else {
                throw new InvalidDataExeption();
            }
        }else{
            field.set(o, x);
        }
    }

    static void length(Field field, Object o, String x) throws IllegalAccessException, InvalidDataExeption {
        if(field.isAnnotationPresent(Length.class)){
            if(x.length() >= field.getAnnotation(Length.class).min() && x.length() <= field.getAnnotation(Length.class).max()) {
                field.set(o, x);
            }else {
                throw new InvalidDataExeption();
            }
        }else{
            field.set(o, x);
        }
    }
}
