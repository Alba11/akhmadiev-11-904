package KFU.ITIS.src.s2.hw11;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main11 {
    public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException, InvalidDataExeption {
        List<String> strings = new ArrayList<>();
        Stream<String> stringStream = Files.lines(Paths.get("KFU/ITIS/src/s2/hw11/input"));

        strings = stringStream.collect(Collectors.toList());
        Pattern pattern = Pattern.compile("\"-?\\w+\"");
        Matcher matcher = pattern.matcher(strings.get(0));
        Class<?> cl;
        if (matcher.find()) {
            cl = Class.forName("KFU.ITIS.src.s2.hw11." + matcher.group().replace("\"", ""));
            Object object = cl.getConstructor().newInstance();
            for (int i = 1; i < strings.size() - 1; i++) {
                matcher = pattern.matcher(strings.get(i));
                if (matcher.find()) {
                    Field field = cl.getDeclaredField(matcher.group(0).replace("\"", ""));
                    field.setAccessible(true);
                    if (matcher.group(0).replace("\"", "").equals("id")) {
                        if (matcher.find()) {
                            ChekAnn.positive(field, object, Integer.parseInt(matcher.group(0).replace("\"", "")));
                        } else {
                            System.out.println("InvalidJson");
                        }
                    } else {
                        if (matcher.find()) {
                            ChekAnn.length(field, object, matcher.group(0).replace("\"", ""));
                        } else {
                            System.out.println("InvalidJson");
                        }
                    }
                } else {
                    System.out.println("InvalidJson");
                }
            }
            Method method = cl.getDeclaredMethod("print");
            method.setAccessible(true);
            method.invoke(object);
        } else {
            System.out.println("InvalidJson");
        }

    }
}

class User {
    @Positive
    private int id;

    @Length(min = 4, max = 100)
    private String firstName;
    private String lastName;
    private String email;

    public User() {
    }

    public User(int id, String firstName, String lastName, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    private void print() {
        System.out.println("User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                '}');
    }
}

class Book {
    @Positive
    private int id;

    @Length(min = 4, max = 11)
    private String title;

    private String author;

    public Book() {
    }

    public Book(int id, String title, String author) {
        this.id = id;
        this.title = title;
        this.author = author;
    }

    private void print() {
        System.out.println("Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                '}');
    }
}
