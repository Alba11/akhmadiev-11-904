package KFU.ITIS.src.s2.hw9;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;

public class Main2 {
    public static void main(String[] args) throws IOException {
        String address = "https://habr.com/ru/";
        URL url = new URL(address);
        InputStream is;

        Document doc = Jsoup.parse(url, 5000);

        Elements collections1 = doc.select("link[abs:href$=.css]");
        System.out.println("style size = " + collections1.size());
        int i = 0;
        for (Element link : collections1) {
            try {
                i++;
                is = new URL(link.attr("abs:href")).openStream();
                Files.copy(is, new File("KFU/ITIS/src/s2/hw9/download2/style" + i + ".css").toPath());
                link.attr("href", "download2/style" + i + ".css");
            } catch (IOException ignored) {

            }
        }
        Elements collections2 = doc.select("script[abs:src$=.js]");
        System.out.println("js size = " + collections2.size());
        for (Element link : collections2) {
            try {
                i++;
                is = new URL(link.attr("abs:src")).openStream();
                Files.copy(is, new File("KFU/ITIS/src/s2/hw9/download2/skript" + i + ".js").toPath());
                link.attr("href", "download2/skript" + i + ".js");
            } catch (IOException ignored) {

            }
        }
        Elements collections3 = doc.select("img[src]");
        System.out.println("img size = " + collections3.size());
        for (Element link : collections3) {

            try {
                i++;
                String u = link.attr("href");
                is = new URL(link.attr("abs:src")).openStream();
                Files.copy(is, new File("KFU/ITIS/src/s2/hw9/download2/image" + i + "." + "png").toPath());
                link.attr("href", "download2/image" + i + "." + "png");
            } catch (IOException ignored) {

            }
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter("./KFU/ITIS/src/s2/hw9/index.html"));
        writer.write(doc.toString());
    }
}

