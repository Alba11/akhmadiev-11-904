package KFU.ITIS.src.s2.hw9;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;


public class Main1 {

    public static void main(String[] args) throws IOException {
        URL url = new URL("https://ru.wikipedia.org/wiki/PNG");
        System.out.println("URl: " + url);
        String find = ".png";

        Document doc = Jsoup.parse(url, 5000);

        Elements collections1 = doc.getElementsByAttributeValueEnding("href", find);
        Elements collections2 = doc.getElementsByAttributeValueEnding("src", find);
        System.out.println("___________________________________");
        System.out.println("a size = " + collections1.size());
        int i = 0;
        for(Element a : collections1){
            try {
                i++;
                InputStream is = new URL(a.attr("abs:href")).openStream();
                Files.copy(is, new File("KFU/ITIS/src/s2/hw9/download2/a" + i + find).toPath());
            }catch (IOException ignored){

            }
        }

        System.out.println("___________________________________");
        System.out.println("img size = " + collections2.size());
        for(Element a : collections2){
            try {
                i++;
                InputStream is = new URL(a.attr("abs:src")).openStream();
                Files.copy(is, new File("KFU/ITIS/src/s2/hw9/download2/image" + i + find).toPath());
            }catch (IOException ignored){

            }
        }

    }
}
