package KFU.ITIS.src.s2.hw12;

public class StringConversion {

    public static String flipString(String s) throws InvalidString {
        if(s == null) throw new InvalidString();
        return reverse(s, s.length());
    }

    private static String reverse(String s, int fin){
        String result = "";
        int first = fin;
        int start = 0;

        for(int i = first - 1; i >= 0; i--){
            if(s.charAt(i) == ' ') {
                if(s.charAt(i-1) != ' ') fin = i;
                continue;
            }
            if(i == 0){
                if(s.charAt(i) == ' ') return "";
                first = 0;
                break;
            }
            if(s.charAt(i) != ' ' && s.charAt(i-1) == ' '){
                first = i;
                break;
            }
        }

        for (int i = first; i < fin; i++) {
            result += s.charAt(i);
        }

        for(int i = first - 1; i >= 0; i--){
            if(s.charAt(i) != ' '){
                first = i+1;
                break;
            }
            if(i == 0) return result;
        }

        if(first > 0){
            result+=" ";
            result += reverse(s, first  );
        }

        return result;
    }

    public static int parseInt(String s) throws InvalidString {
        if(s == null) throw new InvalidString();
        int digit = 1;
        int lastIndex = 0;
        int sum = 0;
        final int MIN_Int = -2147483648;
        final int MAX_Int = 2147483647;

        if(s.charAt(0) >= '0' && s.charAt(0) <= '9'){
            for(int i = 1; i < s.length(); i ++){
                if(s.charAt(i) >= '0' && s.charAt(i) <= '9'){
                    if(i == s.length()-1) {
                        lastIndex = i;
                        break;
                    }
                    continue;
                }
                if(s.charAt(i) == ' '){
                    lastIndex = i - 1;
                    break;
                }
                return 0;
            }

            for(int i = lastIndex; i >= 0; i--){
                int mid = (s.charAt(i) - '0')*digit;
                if(mid > MAX_Int - sum){
                    return MAX_Int;
                }
                sum+=mid;
                digit *= 10;
            }
            return sum;
        }else if(s.charAt(0) == '-'){

            for(int i = 1; i < s.length(); i ++){
                if(s.charAt(i) >= '0' && s.charAt(i) <= '9'){
                    if(i == s.length()-1) {
                        lastIndex = i;
                        break;
                    }
                    continue;
                }
                if(s.charAt(i) == ' '){
                    lastIndex = i - 1;
                    break;
                }
                return 0;
            }

            for(int i = lastIndex; i > 0; i--){
                int mid = (s.charAt(i) - '0')*digit;
                if(-mid < MIN_Int - sum){
                    return MIN_Int;
                }
                sum-=mid;
                digit *= 10;
            }
            return sum;
        }
        return 0;
    }
}
