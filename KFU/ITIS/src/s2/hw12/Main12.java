package KFU.ITIS.src.s2.hw12;

import java.util.Scanner;

public class Main12 {
    public static void main(String[] args) throws InvalidString {
        Scanner scanner = new Scanner(System.in);
        String s1 = scanner.nextLine();
        String answer1 = StringConversion.flipString(s1);
        System.out.println(answer1);

        String s2 = scanner.nextLine();
        int answer2 = StringConversion.parseInt(s2);
        System.out.println(answer2);
    }
}
