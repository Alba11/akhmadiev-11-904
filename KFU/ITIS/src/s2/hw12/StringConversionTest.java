package KFU.ITIS.src.s2.hw12;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class StringConversionTest {

    @Test
    void flipString() throws InvalidString {
        String s1 = "best the are We";
        String exceptedResult1 = "We are the best";
        String actualResult1 = StringConversion.flipString(s1);
        Assertions.assertEquals(exceptedResult1, actualResult1);

        String s2 = "       1       2  3              4              6                      ";
        String exceptedResult2 = "6 4 3 2 1";
        String actualResult2 = StringConversion.flipString(s2);
        Assertions.assertEquals(exceptedResult2, actualResult2);
    }

    @Test
    void flipString_InvalidString() throws InvalidString {
        String s = null;
        Assertions.assertThrows(InvalidString.class, ()-> StringConversion.flipString(s));
    }

    @Test
    void parceInt() throws InvalidString {
        String s1 = "111";
        int exceptedResult1 = 111;
        int actualResult1 = StringConversion.parseInt(s1);
        Assertions.assertEquals(exceptedResult1, actualResult1);

        String s2 = "11113245453245364753232 l;ksd";
        int exceptedResult2 = Integer.MAX_VALUE;
        int actualResult2 = StringConversion.parseInt(s2);
        Assertions.assertEquals(exceptedResult2, actualResult2);

        String s3 = "-11113245453245364753232 dshfghskf";
        int exceptedResult3 = Integer.MIN_VALUE;
        int actualResult3 = StringConversion.parseInt(s3);
        Assertions.assertEquals(exceptedResult3, actualResult3);

        String s4 = " aSFK 11";
        int exceptedResult4 = 0;
        int actualResult4 = StringConversion.parseInt(s4);
        Assertions.assertEquals(exceptedResult4, actualResult4);

        String s5 = "11,2";
        int exceptedResult5 = 0;
        int actualResult5 = StringConversion.parseInt(s5);
        Assertions.assertEquals(exceptedResult5, actualResult5);

        String s6 = "-1245 ;rekg ";
        int exceptedResult6 = -1245;
        int actualResult6 = StringConversion.parseInt(s6);
        Assertions.assertEquals(exceptedResult6, actualResult6);

        String s7 = "-11,2";
        int exceptedResult7 = 0;
        int actualResult7 = StringConversion.parseInt(s7);
        Assertions.assertEquals(exceptedResult7, actualResult7);
    }

    @Test
    void parceInt_InvalidString() throws InvalidString {
        String s = null;
        Assertions.assertThrows(InvalidString.class, ()-> StringConversion.parseInt(s));
    }
}