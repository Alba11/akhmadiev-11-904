package KFU.ITIS.src.s2.semWork2;

import java.util.LinkedList;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();

        BucketSorter bucketSorter = new BucketSorter();

        LinkedList<Integer> sort = new LinkedList<>();

        for(int i = 1; i < 100; i++) {
            sort.add(random.nextInt(i));
        }
        System.out.println("Изначальные данные: " + sort);
        System.out.println("Отсортированный данные: " + bucketSorter.sort(sort));

        System.out.println();

        LinkedList<Integer> list;
        int n = 100001;
        int s = 10000;
        int rep = 10;


        System.out.println("{Хорошие данные: ");
        for(int i = s; i < n; i += s){
            long sum = 0;
            for(int j = 0; j < rep; j++) {
                list = new LinkedList<>();
                for (int a = 1; a < i; a ++) {
                    list.add(random.nextInt(a));
                }
                long start = System.nanoTime();
                bucketSorter.sort(list);
                long  finish = System.nanoTime();
                long time = finish - start;
                sum+=time;
            }
            System.out.println(i + " " +  sum/rep);
        }

        System.out.println();

        System.out.println("Плохие данные: ");
        for(int i = s; i < n; i += s){
            long sum = 0;
            for(int j = 0; j < rep; j++) {
                list = new LinkedList<>();
                list.add(1);
                for (int a = 1; a < i; a ++) {
                    list.add(i-100);
                }
                long start = System.nanoTime();
                bucketSorter.sort(list);
                long  finish = System.nanoTime();
                long time = finish - start;
                sum+=time;
            }
            System.out.println(i + " " +  sum/rep);
        }
    }
}
