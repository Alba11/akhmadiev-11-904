package KFU.ITIS.src.s2.bonus.dp1;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter URL:");
        String stringURL = sc.nextLine().trim();
        stringURL = stringURL.trim();
        System.out.println("Enter type:");
        String type = sc.nextLine().trim();
        System.out.println("Enter count of Threads");
        int count = sc.nextInt();

        ExecutorService executor = Executors.newFixedThreadPool(count);
        try {
            Document document = Jsoup.parse(new URL(stringURL), 10000);
            Elements collection = document.select("img[src$=" + type + "]");

            int id = 0;
            for (Element element: collection) {
                id ++;
                int finalId = id;
                executor.submit(()->{
                    try {
                        URL url = new URL(element.absUrl("src"));
                        InputStream inputStream = url.openStream();
                        Files.copy(inputStream, new File("KFU/ITIS/src/s2/bonus/dp1/images/" + finalId + "_" + Thread.currentThread().getName() + type).toPath());
                    } catch (MalformedURLException e) {
                        System.out.println("Incorrect URL");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
            executor.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

