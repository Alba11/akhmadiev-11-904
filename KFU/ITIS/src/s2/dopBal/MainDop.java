package KFU.ITIS.src.s2.dopBal;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainDop {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Scanner sc = new Scanner(System.in);
        String className = sc.nextLine();

        try {
            Class<?> cl = Class.forName(className);

            System.out.println("Task 7: ");

            Map<Object, List<Field>> map = Arrays.stream(cl.getFields())
                    .collect(Collectors.groupingBy(Field::getType));
            for (Map.Entry<Object, List<Field>> entry : map.entrySet()) {
                System.out.print(entry.getKey() + ": ");
                entry.getValue().forEach(x -> System.out.print(x.getName() + "; "));
                System.out.println();
            }
            System.out.println("____________________________");

            System.out.println("Task 8: ");

            String name = sc.nextLine();
            Class<?> newClass = Class.forName(name);
            Object o = newClass.getConstructor().newInstance();

            int n = sc.nextInt();
            sc.nextLine();
            String[] lines = new String[n];
            for (int i = 0; i < n; i++) {
                lines[i] = sc.nextLine();
            }

            Arrays.stream(lines).map(x -> x.split(" ")).forEach(splits -> {
                try {
                    Field field = newClass.getDeclaredField(splits[1]);
                    field.setAccessible(true);
                    switch (splits[0]){
                        case "String":
                            field.set(o,  splits[2]);
                            break;
                        case "int":
                            field.set(o,  Integer.parseInt(splits[2]));
                            break;
                        case "double":
                            field.set(o,  Double.parseDouble(splits[2]));
                            break;
                        case "float":
                            field.set(o,  Float.parseFloat(splits[2]));
                            break;
                        case "boolean":
                            field.set(o,  Boolean.parseBoolean(splits[2]));
                            break;
                        case "char":
                            field.set(o,  splits[2].charAt(0));
                            break;
                        case "long":
                            field.set(o,  Long.parseLong(splits[2]));
                            break;
                        case "short":
                            field.set(o,  Short.parseShort(splits[2]));
                            break;
                        case "byte":
                            field.set(o,  Byte.parseByte(splits[2]));
                            break;
                    }
                } catch (IllegalAccessException | NoSuchFieldException e) {
                    e.printStackTrace();
                }
            });
            System.out.println(o.toString());
        } catch (Exception ex) {
            System.out.println("Class not found ");
        }
    }
}

class User {
    private String userName;
    private int id;
    private int year;
    public String email;
    public boolean active;
    public int dayOfRegistr;

    public User(){

    }

    private User(int id, String name) {
        this.id = id;
        this.userName = name;
    }

    public User(int id, String name, int year, String email) {
        this.id = id;
        this.userName = name;
        this.year = year;
        this.email = email;
    }

    public void print() {
        System.out.println("year = " + year + "email: " + email);
    }


    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", id=" + id +
                ", year=" + year +
                ", email='" + email + '\'' +
                ", active=" + active +
                ", dayOfRegistr=" + dayOfRegistr +
                '}';
    }
}