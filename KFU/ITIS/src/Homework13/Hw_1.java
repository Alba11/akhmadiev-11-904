package Homework13;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Hw_1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        String result;
        Pattern p1 = Pattern.compile("^(0[13478]|1[02])/(0[1-9]|[12][0-9]|3[01])/([0-9]{4})\\s(0[1-9]|1[0-9]|2[0-3]):([0-5][0-9])$");
        Pattern p2 = Pattern.compile("^(0[469]|11)/(0[1-9]|[12][0-9]|3[0])/([0-9]{4})\\s(0[1-9]|1[0-9]|2[0-3]):([0-5][0-9])$");
        Pattern p3 = Pattern.compile("^([0][2]/(0[1-9]|[1][0-9]|2[0-8])/([0-9]{4})\\s(0[1-9]|1[0-9]|2[0-3]):([0-5][0-9]))$");

        Pattern p4 = Pattern.compile("^(0[13478]|1[02])/(0[1-9]|[12][0-9]|3[01])/([0-9]{2})\\s(0[1-9]|1[0-9]|2[0-3]):([0-5][0-9])$");
        Pattern p5 = Pattern.compile("^(0[469]|11)/(0[1-9]|[12][0-9]|3[0])/([0-9]{2})\\s(0[1-9]|1[0-9]|2[0-3]):([0-5][0-9])$");
        Pattern p6 = Pattern.compile("^([0][2]/(0[1-9]|[1][0-9]|2[0-8])/([0-9]{2})\\s(0[1-9]|1[0-9]|2[0-3]):([0-5][0-9]))$");

        Pattern p7 = Pattern.compile("^(JUN|MAR|MAY|JUL|AUG|OCT|DEC)/(0[1-9]|[12][0-9]|3[01])/([0-9]{4})\\s(0[1-9]|1[0-9]|2[0-3]):([0-5][0-9])$");
        Pattern p8 = Pattern.compile("^(APR|JUN|SEP|NOV)/(0[1-9]|[12][0-9]|3[0])/([0-9]{4})\\s(0[1-9]|1[0-9]|2[0-3]):([0-5][0-9])$");
        Pattern p9 = Pattern.compile("^(FEB)/(0[1-9]|[1][0-9]|2[0-8])/([0-9]{4})\\s(0[1-9]|1[0-9]|2[0-3]):([0-5][0-9])$");

        Matcher m1 = p1.matcher(s);
        Matcher m2 = p2.matcher(s);
        Matcher m3 = p3.matcher(s);
        Matcher m4 = p4.matcher(s);
        Matcher m5 = p5.matcher(s);
        Matcher m6 = p6.matcher(s);
        Matcher m7 = p7.matcher(s);
        Matcher m8 = p8.matcher(s);
        Matcher m9 = p9.matcher(s);

        if (m1.find()||m2.find()||m3.find()||m4.find()||m5.find()||m6.find()||m7.find()||m8.find()||m9.find()){
            result = "DATE";
        }else{
            result = "NO DATE";
        }
        System.out.println(result);
    }
}
