package Homework13;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Hw_2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        Pattern p = Pattern.compile("\\b[A-Za-z0-9._]+@[A-Za-z0-9]+\\.[A-Za-z]{2,4}\\b");
        Matcher m = p.matcher(s);
        int t = 0;
         while(m.find()){
             System.out.println(s.substring(m.start(), m.end()));
             t++;
         }
         if(t == 0){
             System.out.println("Mailbox not found");
         }
    }
}
