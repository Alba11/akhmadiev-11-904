package Homework20;

import java.util.Random;

public class Matrix {
    Random r = new Random();
    private int n;
    private int m;
    private int[][] a;

    public Matrix(int n, int m) {
        this.n = n;
        this.m = m;
        int[][] b = new int[n][m];
        a = b;
    }

    public int nLenght() {
        return n;
    }

    public int mLenght() {
        return m;
    }

    public void createRandomMatrix() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = r.nextInt(5) + 1;
            }
        }
    }

    public void printMatrix() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }

    public int getValue(int i, int j) {
        return a[i][j];
    }

    public void setValue(int i, int j, int result) {
        a[i][j] = result;
    }
}
