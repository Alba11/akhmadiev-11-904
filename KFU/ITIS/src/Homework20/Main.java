package Homework20;

import java.util.Scanner;
import java.util.function.BinaryOperator;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Введите чилсо строк для первой матрицы:");
        int nFirst = sc.nextInt();
        System.out.println("Введите чилсо столбцов для первой матрицы:");
        int mFirst = sc.nextInt();
        Matrix matrixOne = new Matrix(nFirst, mFirst);
        matrixOne.createRandomMatrix();
        matrixOne.printMatrix();

        System.out.println("Введите чилсо строк для второй матрицы:");
        int nSecond = sc.nextInt();
        System.out.println("Введите чилсо столбцов для второй матрицы:");
        int mSecond = sc.nextInt();
        Matrix matrixTwo = new Matrix(nSecond, mSecond);
        matrixTwo.createRandomMatrix();
        matrixTwo.printMatrix();

        if (mFirst != nSecond) {
            System.out.println("Такие матрицы перемножить невозможно!");
        } else {

            BinaryOperator<Matrix> matrixMultiplication = (matrix1, matrix2) -> {
                Matrix result = new Matrix(nFirst, mSecond);
                for (int i = 0; i < matrix1.nLenght(); i++) {
                    for (int j = 0; j < matrix2.mLenght(); j++) {
                        int s = 0;
                        for (int p = 0; p < matrix1.mLenght(); p++) {
                            s += matrix1.getValue(i, p) * matrix2.getValue(p, j);
                            result.setValue(i, j, s);
                        }
                    }
                }
                return result;
            };
            System.out.println();
            System.out.println("Ответ: ");
            matrixMultiplication.apply(matrixOne, matrixTwo).printMatrix();

        }
    }
}
