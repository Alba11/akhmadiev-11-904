package Test2;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task2 {
    public static void main(String[] args) {
        FileInputStream fis =null;

        try {
            fis = new FileInputStream("D:\\HomeWork\\akhmadiev-11-904\\KFU\\ITIS\\src\\Test2\\input.txt");

            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("D:\\HomeWork\\akhmadiev-11-904\\KFU\\ITIS\\src\\Test2\\output_AkhmadievAlbert.txt"), "UTF8" ));
            int i = 0;
            double s1 = 0, s2 = 0;

            while ((i = fis.read()) != -1) {
                if (i == (int) '.' || i == (int) ':' || i == (int) '!' || i == (int) '?' || i == (int) ',' || i == (int) ';' || i == (int) '"' || i == (int) '\'' || i == (int) ']' || i == (int) '[' || i == (int) '{' || i == (int) '}' || i == (int) '(' || i == (int) ')') {
                    s2 += 1;
                }
                s1 += 1;
            }
            double ans = s2 / s1 * 100;
            String s = Double.toString(ans);
            System.out.print(ans);
            out.append(s).append("%");
            out.flush();
            out.close();
        }catch (IOException ex){
            System.out.print(ex);
        }
        finally {
            try {
                fis.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
