package Test2;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task3 {
    public static void main (String[] args) {
        Scanner sc = new Scanner (System.in);
        String str = sc.nextLine();

        Pattern p = Pattern.compile("([0-9]+[,.][0-9]+) | ([0-9]+) ");

        Matcher m = p.matcher(str);
        double sum = 0;
        while (m.find()) {
            int start = m.start();
            int end = m.end();
            String s = str.substring(start,end);
            double i = Double.parseDouble(s);
            sum = sum + i;
            System.out.print(i + " + ");
        }
        System.out.print("= " + sum);
    }
}
