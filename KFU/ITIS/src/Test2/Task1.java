package Test2;

import java.util.Random;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();

        int n = sc.nextInt();
        int m = sc.nextInt();

        int[][] a = new int[n][m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = r.nextInt(10) + 1;
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        int s1 = 0;
        int s2 = 0;

        for (int q = 0; q < m - 1; q++) {
            for (int i = 0; i < m - 1; i++) {
                for (int j = 0; j < n; j++) {
                    if (a[j][i] % 2 == 0) {
                        s1++;
                    }
                    if (a[j][i + 1] % 2 == 0) {
                        s2++;
                    }
                }
                if (s1 > s2) {
                    for (int j = 0; j < n; j++) {
                        int p = a[j][i];
                        a[j][i] = a[j][i + 1];
                        a[j][i + 1] = p;
                    }
                }
                s1 = 0;
                s2 = 0;
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }

    }
}
