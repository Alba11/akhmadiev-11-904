package KFU.ITIS.s2.homeworks.hw8;

import javax.crypto.spec.PSource;
import javax.naming.OperationNotSupportedException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.StringTokenizer;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws IOException {

        System.out.println("Task №1: ");
        Stream<String> stringStream1 = Files.lines(Paths.get("KFU/ITIS/s2/homeworks/hw8/alba"));
        stringStream1.distinct().filter(x->x.trim().split("\\s+").length >2).forEach(System.out::println);
        System.out.println();

        System.out.println("Task #2: ");
        Stream<String> stringStream2 = Files.lines(Paths.get("KFU/ITIS/s2/homeworks/hw8/alba"));
        System.out.println(stringStream2.mapToInt(String::length).min().orElse(-1));
        System.out.println();

        System.out.println("Task №3: ");
        Stream<String> stringStream3 = Files.lines(Paths.get("KFU/ITIS/s2/homeworks/hw8/alba"));
        System.out.println(stringStream3.mapToInt(x -> x.trim().split("\\s+").length).average());
    }
}


