package KFU.ITIS.s2.homeworks.hw1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("D:\\HomeWork\\akhmadiev-11-904\\KFU\\ITIS\\s2\\homeworks\\hw1\\Names"));
        Table table = new Table();

        while (sc.hasNext()) {
            try {
                String str = sc.nextLine();
                int data = table.parseDate(str);
                switch (data) {
                    case -1:
                        throw new IncorrectInfoExeption();
                    case -2:
                        throw new NoDataExeption();
                }
                int age = 2020 - data;
                System.out.println(age);
            } catch (IncorrectInfoExeption incorrectInfoExeption) {
                System.out.print("Error string ");
                incorrectInfoExeption.printStackTrace();
            } catch (NoDataExeption noDataExeption) {
                System.out.print("Error data ");
                noDataExeption.printStackTrace();
            }

        }
    }
}
