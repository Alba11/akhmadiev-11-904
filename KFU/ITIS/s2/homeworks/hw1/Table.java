package KFU.ITIS.s2.homeworks.hw1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Table {
    private Pattern infoPattern = Pattern.compile("^[A-Za-z0-9]*[;][\\s]*[0-9]*[;]$");

    private Pattern datePattern = Pattern.compile("[;]\\s*[0-9]*\\s*[;]");


    int parseDate(String str) {

        Matcher m1 = infoPattern.matcher(str);

        if (m1.find()) {
            Matcher m2 = datePattern.matcher(str);
            m2.find();
            String res = str.substring(m2.start() + 1, m2.end() - 1).trim();
            int data = Integer.parseInt(res);
            if (data > 0 && data < 2021) {
                return data;
            } else {
                return -2;
            }
        } else {
            return -1;
        }
    }

}
