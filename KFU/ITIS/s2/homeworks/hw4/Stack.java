package KFU.ITIS.s2.homeworks.hw4;

public class Stack<Double> {
    private static class Element<T> {
        private double value;
        private Element<T> next;

        Element(double value) {
            this.value = value;
        }
    }

    private Element<Double> topElement;

    public Element<Double> getTopElement() {
        return topElement;
    }

    private int size = 0;

    public void push(double value) {
        Element<Double> newEl = new Element<>(value);
        newEl.next = topElement;
        topElement = newEl;
        size++;
    }

    public boolean isEmpty() {
        return topElement == null;
    }

    public double top() {
        return topElement != null ? topElement.value : null;
    }

    public void topDelete() {
        if (topElement != null) {
            topElement = topElement.next;
            size--;
        }
    }

    public void printAll() {
        Element<Double> nextElement = topElement;
        while (nextElement != null) {
            System.out.print(nextElement.value + " ");
            nextElement = nextElement.next;
        }
        System.out.println();
    }

    public void countStack(char c) {
        if (size > 1) {
            Element<Double> nextElement = topElement.next;
            switch (c) {
                case '+':
                    nextElement.value += topElement.value;
                    topElement = nextElement;
                    break;
                case '*':
                    nextElement.value *= topElement.value;
                    topElement = nextElement;
                    break;
                case '-':
                    nextElement.value -= topElement.value;
                    topElement = nextElement;
                    break;
                case '/':
                    nextElement.value /= topElement.value;
                    topElement = nextElement;
                    break;
            }
            size--;
        } else {
            System.out.println("Error number");
        }
    }

    public int getSize() {
        return size;
    }
}
