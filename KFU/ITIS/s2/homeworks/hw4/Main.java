package KFU.ITIS.s2.homeworks.hw4;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {
    public static void main(String[] args){

        System.out.println("First homework: ");
        Queue<Integer> queue1 = new Queue<>();
        Queue<Integer> queue2 = new Queue<>();
        Queue ansQueue = new Queue<>();

        queue1.addMore(5, 4, 30, 2, 5, 6, 7, 100);
        queue1.printAll();
        queue2.addMore(1, 2, 3, 5, 1, 2, 1000, 10, 333, 777, 1, 0);
        queue2.printAll();

        ansQueue = ansQueue.sort(queue1, queue2);
        ansQueue.printAll();
        System.out.println();

        System.out.println("Second homework: ");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        StringTokenizer st = new StringTokenizer(s, ",");

        Stack<Double> stack = new Stack<>();
        while (st.hasMoreElements()) {
            String token = st.nextToken().trim();
            if (token.length() == 1 && (token.charAt(0) == '+' || token.charAt(0) == '*' || token.charAt(0) == '-' || token.charAt(0) == '/')) {
                stack.countStack(token.charAt(0));
            } else {
                stack.push(Double.parseDouble(token));
            }
        }
        if (stack.isEmpty() || stack.getSize() != 1) {
            System.out.println("Error string");
        } else {
            System.out.println(stack.top());
        }
    }
}
