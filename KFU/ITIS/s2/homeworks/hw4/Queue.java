package KFU.ITIS.s2.homeworks.hw4;

public class Queue<Integer> {
    private static class Element<T> {
        private T value;
        private Element<T> next;
        private Element<T> prev;

        Element(T value) {
            this.value = value;
        }
    }

    private Element<Integer> firstElement;
    private Element<Integer> lastElement;
    private int size = 0;

    public void printAll() {
        Element<Integer> nextElement = firstElement;
        System.out.print("Size = " + size + " elements : ");
        while (nextElement != null) {
            System.out.print(nextElement.value + " ");
            nextElement = nextElement.next;
        }
        System.out.println();
    }

    public void addMore(Integer... t) {
        for (Integer value : t) {
            addRight(value);
        }
    }

    public void addRight(Integer value) {
        Element<Integer> newElement = new Element<Integer>(value);
        size++;
        if (firstElement == null) {
            firstElement = newElement;
            lastElement = newElement;
        } else {
            if ((int)newElement.value <= (int)firstElement.value) {
                newElement.next = firstElement;
                firstElement.prev = newElement;
                firstElement = newElement;
            } else if ((int)newElement.value >= (int)lastElement.value) {
                lastElement.next = newElement;
                newElement.prev = lastElement;
                lastElement = newElement;
            } else if (size == 2) {
                firstElement.next =newElement;
                newElement.next = lastElement;
                lastElement.prev = newElement;
                newElement.prev = firstElement;
            } else {
                Element<Integer> nextElement = firstElement.next;
                while (nextElement != null) {
                    if ((int)newElement.value <= (int)nextElement.value) {
                        nextElement.prev.next = newElement;
                        newElement.prev = nextElement.prev;
                        newElement.next = nextElement;
                        nextElement.prev = newElement;
                        break;
                    }
                    nextElement = nextElement.next;
                }
            }
        }
    }

    public void push(Integer value){
        Element<Integer> newElement = new Element<>(value);
        if(firstElement == null){
            firstElement = newElement;
            lastElement = newElement;
        }else{
            lastElement.next = newElement;
            newElement.prev = lastElement;
            lastElement = newElement;
        }
        size++;
    }

    public void deleteFirst(){
        firstElement.next.prev = null;
        firstElement = firstElement.next;
    }

    public void pop() {
        deleteFirst();
    }

    public boolean isEmpty(){
        if(firstElement != null){
            return true;
        }
        return false;
    }

    public Queue sort(Queue<Integer> queue1, Queue<Integer> queue2){
        Queue<Integer> ans = new Queue<>();
        Element<Integer> nextElement1 = queue1.firstElement;
        Element<Integer> nextElement2 = queue2.firstElement;
        while (nextElement1 != null && nextElement2 != null){
            if((int)nextElement1.value < (int)nextElement2.value) {
                ans.push(nextElement1.value);
                nextElement1 = nextElement1.next;
            } else {
                ans.push(nextElement2.value);
                nextElement2 = nextElement2.next;
            }
        }
        while (nextElement1 != null){ ans.push(nextElement1.value); nextElement1 = nextElement1.next;}
        while (nextElement2 != null){ ans.push(nextElement2.value); nextElement2 = nextElement2.next;}
        return ans;
    }
}



