package KFU.ITIS.s2.homeworks.hw3;

import java.time.LocalDateTime;

public class TextAdValidator {
    public static boolean textAdValidator(Add<? extends TextContent> add){
        if(add.getContent().getText().length() <= 0 || add.getContent().getText().length() > 100 ){
            System.out.println("Your title is so long"); return false;}
        if(add.getContent().getTitle().length() <= 0 || add.getContent().getTitle().length() > 30){
            System.out.println("Your text is so long"); return false;}
        LocalDateTime now = LocalDateTime.now();
        if(add.getStartDateTime().isAfter(now)) {System.out.println("You enter future date"); return false;}
        if(add.getStartDateTime().isBefore(now.minusDays(30))) {System.out.println("Your add is out of time"); return false;}
        return true;
    }
}
