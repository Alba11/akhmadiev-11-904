package KFU.ITIS.s2.homeworks.hw3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the title of add: ");
        String title = sc.nextLine();
        System.out.println("Enter the text of add: ");
        String text = sc.nextLine();
        System.out.println("Enter the data of creating, for example: 22-05-2001 07:30");
        String data = sc.nextLine();

        Content content = new TextContent(title, text);
        PhotoContent pContent = new PhotoContent(title, text);
        Add add = new Add(data);
        add.setContent(pContent);
        System.out.println(TextAdValidator.textAdValidator(add));
        System.out.println(add.getStartDateTime());
    }
}
