package KFU.ITIS.s2.homeworks.hw3;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Add<T extends Content> {
    private T content;
    private LocalDateTime startDateTime;

    public Add(String date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        this.startDateTime = LocalDateTime.parse(date, formatter);
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }
}
