package KFU.ITIS.s2.homeworks.hw3;

import java.io.File;

public abstract class Content {
    private String title;

    Content(String title){
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        }
}

class TextContent extends Content{
    private String text;

    TextContent(String title, String text) {
        super(title);
        this.text = text;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

class PhotoContent extends TextContent{
    private File photo = null;

    PhotoContent(String title, String text) {
        super(title, text);
    }

    public void setPhoto(File photo) {
        this.photo = photo;
    }
}
