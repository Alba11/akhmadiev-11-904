package KFU.ITIS.s2.homeworks.hw5;

public class Main {
    public static void main(String[] args) {
        ArrayStack<Integer> arrayStack = new ArrayStack<>(5);

        System.out.println(arrayStack.top());
        arrayStack.push(1);
        arrayStack.push(2);
        arrayStack.push(3);
        arrayStack.push(4);
        arrayStack.push(5);
        for(int a:arrayStack){
            System.out.print(a + " ");
        }
        System.out.println();

        System.out.println(arrayStack.top());

        System.out.println(arrayStack.pop());

        for(int a:arrayStack){
            System.out.print(a + " ");
        }
    }
}
