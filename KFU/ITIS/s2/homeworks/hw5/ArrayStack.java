package KFU.ITIS.s2.homeworks.hw5;

import KFU.ITIS.s2.lessons.lesson5.ArrayElementCollection;

import java.util.Arrays;
import java.util.Iterator;

public class ArrayStack<T> implements Iterable<T>{

    private Object[] arr;
    private int emptySize;

    ArrayStack(int lenght){
        arr = new Object[lenght];
        emptySize = lenght;
    }

    private boolean isEmpty(){
        if(emptySize == arr.length) return true;
        return false;
    }

    public T top(){
        if(isEmpty()){
            System.out.println("Stack is empty");
            return null;
        }else{
            return (T) arr[arr.length-emptySize - 1];
        }
    }

    public T pop(){
        T a = top();
        arr[arr.length - emptySize - 1] = 0;
        emptySize++;
        return a;
    }

    public void push(T value){
        if (emptySize > 0) {
            arr[arr.length - emptySize] = value;
            emptySize--;
        } else {
            emptySize = arr.length;
            arr = Arrays.copyOf(arr, arr.length * 2 + 1);
            arr[arr.length - emptySize - 1] = value;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayStackIterator<>();
    }

    private class ArrayStackIterator<T> implements Iterator<T> {
        int currentIndex = 0;
        @Override
        public boolean hasNext() {
            return currentIndex<arr.length-emptySize;
        }

        @Override
        public T next() {
            if(hasNext()){
                currentIndex++;
                return (T) arr[arr.length-emptySize-currentIndex];
            }
            return null;
        }
    }


}
