package KFU.ITIS.s2.homeworks.hw6;

import java.util.Comparator;

public class ComparatorFio<T extends Student> implements Comparator<T> {
    @Override
    public int compare(T student1, T student2) {
        if(student1 == null || student2 == null) return -1;
        if(student1.getFio() == null || student2.getFio() == null) return -1;
        return student1.getFio().compareTo(student2.getFio());
    }


}
