package KFU.ITIS.s2.homeworks.hw6;

public class Student {
    private String fio;
    private int year;
    private int averageScore;

    public Student(String fio, int year, int averageScore){
        this.fio = fio;
        this.year = year;
        this.averageScore = averageScore;
    }

    public String getFio() {
        return fio;
    }

    public int getYear() {
        return year;
    }

    public int getAverageScore() {
        return averageScore;
    }


}
