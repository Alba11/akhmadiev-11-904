package KFU.ITIS.s2.homeworks.hw6;

import java.util.ArrayList;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        ComparatorFio<Student> comparatorFio = new ComparatorFio<>();
        Comparator<Student> comparatorYear = new Comparator<>() {
            @Override
            public int compare(Student student1, Student student2) {
                return Integer.compare(student1.getYear(), student2.getYear());
            }
        };

        Comparator<Student> comparatorAvarageScore = (student1, student2) -> {
            return Integer.compare(student2.getAverageScore(), student1.getAverageScore());
        };

        ArrayList<Student> arrayList = new ArrayList<>();
        arrayList.add(new Student(null, 2005, 90));
        arrayList.add(new Student("Rishat", 2011, 92));
        arrayList.add(new Student("Alba", 2001, 100));
        arrayList.add(new Student("Bulat", 1999, 97));
        arrayList.add(new Student("Misha", 1000, 95));

        printAllStudents(arrayList);
        System.out.println();

        sort(arrayList, comparatorFio);
        printAllStudents(arrayList);
        System.out.println();

        sort(arrayList, comparatorAvarageScore);
        printAllStudents(arrayList);
        System.out.println();

        sort(arrayList, comparatorYear);
        printAllStudents(arrayList);
        System.out.println();
    }

    private static  <T> void sort(ArrayList<T> arrayList, Comparator comparator) {
        for (int i = 1; i < arrayList.size(); i++) {
            T help = arrayList.get(i);
            for (int j = 0; j < i; j++) {
                if (comparator.compare(arrayList.get(j), help) > 0) {
                    arrayList.add(j, help);
                    arrayList.remove(i + 1);
                    break;
                }
            }
        }
    }

    private static void printAllStudents(ArrayList<Student> arrayList){
        for(int i = 0; i < arrayList.size(); i ++){
            System.out.println((i+1) + ") Name: " + arrayList.get(i).getFio() + "; " + "Year: " + arrayList.get(i).getYear() + "; " + "AvarageScore: " + arrayList.get(i).getAverageScore() + "; ");
        }
    }
}
