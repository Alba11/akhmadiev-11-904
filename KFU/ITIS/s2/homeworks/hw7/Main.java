package KFU.ITIS.s2.homeworks.hw7;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        Random random = new Random();

        for (int j = 1; j <= 10; j++) {
            LinkedList<Integer> list = new LinkedList<>();
            Stack<Integer> stack = new Stack<>();

            int n = 100000 * j;

            for (int i = 0; i < n; i++) {
                list.add(random.nextInt(n));
                stack.add(random.nextInt(n));
            }

            //push for n elements
            long timePushList = 0;
            long timePushStack = 0;

            for (int i = 0; i < 10; i++) {
                long start = System.nanoTime();
                list.push(random.nextInt(n));
                long finish = System.nanoTime();
                timePushList += finish - start;
            }
            System.out.println("Среднее значение list.push элементов от " + n + " = " + (timePushList / 10));

            for (int i = 0; i < 10; i++) {
                long start = System.nanoTime();
                stack.push(random.nextInt(n));
                long finish = System.nanoTime();
                timePushStack += finish - start;
            }
            System.out.println("Среднее значение stack.push элементов от " + n + " = " + (timePushStack / 10));
            System.out.println();
        }

        //pop for n elements
        for (int j = 1; j <= 10; j++) {
            LinkedList<Integer> list = new LinkedList<>();
            Stack<Integer> stack = new Stack<>();

            int n = 100000 * j;

            for (int i = 0; i < n; i++) {
                list.add(random.nextInt(n));
                stack.add(random.nextInt(n));
            }

            long timePopList = 0;
            long timePopStack = 0;

            for (int i = 0; i < 10; i++) {
                long start = System.nanoTime();
                list.pop();
                long finish = System.nanoTime();
                timePopList += finish - start;
            }
            System.out.println("Среднее значение list.pop элементов от " + n + " = " + (timePopList / 10));

            for (int i = 0; i < 10; i++) {
                long start = System.nanoTime();
                stack.pop();
                long finish = System.nanoTime();
                timePopStack += finish - start;
            }
            System.out.println("Среднее значение stack.pop элементов от " + n + " = " + (timePopStack / 10));
            System.out.println();
        }

        //peek for n elements
        for (int j = 1; j <= 10; j++) {
            LinkedList<Integer> list = new LinkedList<>();
            Stack<Integer> stack = new Stack<>();

            int n = 100000 * j;

            for (int i = 0; i < n; i++) {
                list.add(random.nextInt(n));
                stack.add(random.nextInt(n));
            }

            long timePeekList = 0;
            long timePeekStack = 0;

            for (int i = 0; i < 10; i++) {
                long start = System.nanoTime();
                list.peek();
                long finish = System.nanoTime();
                timePeekList += finish - start;
            }
            System.out.println("Среднее значение list.peek элементов от " + n + " = " + (timePeekList / 10));

            for (int i = 0; i < 10; i++) {
                long start = System.nanoTime();
                stack.peek();
                long finish = System.nanoTime();
                timePeekStack += finish - start;
            }
            System.out.println("Среднее значение stack.peek элементов от " + n + " = " + (timePeekStack / 10));
            System.out.println();
        }

        //addAll for n elements
        for (int j = 1; j <= 10; j++) {
            LinkedList<Integer> list = new LinkedList<>();
            Stack<Integer> stack = new Stack<>();

            int n = 100000 * j;

            for (int i = 0; i < n; i++) {
                list.add(random.nextInt(n));
                stack.add(random.nextInt(n));
            }

            long timeAddAllList = 0;
            long timeAddAllStack = 0;
            Collection<Integer> collection = new ArrayList<>();

            for (int i = 0; i < 1000; i++) {
                collection.add(i);
            }

            for (int i = 0; i < 10; i++) {
                long start = System.nanoTime();
                list.addAll(collection);
                long finish = System.nanoTime();
                timeAddAllList += finish - start;
            }
            System.out.println("Среднее значение list.addAll элементов от " + n + " = " + (timeAddAllList / 10));

            for (int i = 0; i < 10; i++) {
                long start = System.nanoTime();
                stack.addAll(collection);
                long finish = System.nanoTime();
                timeAddAllStack += finish - start;
            }
            System.out.println("Среднее значение stack.addAll элементов от " + n + " = " + (timeAddAllStack / 10));
            System.out.println();
        }

        // elements += 2
        for (int j = 1; j <= 10; j++) {
            LinkedList<Integer> list = new LinkedList<>();
            Stack<Integer> stack = new Stack<>();

            int n = 100000 * j;

            for (int i = 0; i < n; i++) {
                list.add(random.nextInt(n));
                stack.add(random.nextInt(n));
            }

            long timeList = 0;
            long timeStack = 0;

            for (int i = 0; i < 10; i++) {
                long start = System.nanoTime();
                for (Integer elem : list) {
                    elem += 2;
                }
                long finish = System.nanoTime();
                timeList += finish - start;
            }
            System.out.println("Среднее значение list elem+=2 элементов от " + n + " = " + (timeList / 10));

            for (int i = 0; i < 10; i++) {
                long start = System.nanoTime();
                for (Integer elem : stack) {
                    elem += 2;
                }
                long finish = System.nanoTime();
                timeStack += finish - start;
            }
            System.out.println("Среднее значение stack elem+=2 элементов от " + n + " = " + (timeStack / 10));
            System.out.println();
        }

    }
}
