package KFU.ITIS.s2.homeworks.hw2;

public class ElementCollection<T> implements ElementCollectionOption<T> {

    private int size = 0;

    private static class Element<T> {

        Element(T value) {
            this.value = value;
        }

        private T value;
        private Element<T> next;

    }

    private Element<T> firstElement;

    public void addMore(T... t){
        for (T value : t) {
            addR(value);
        }
    }

    public void printAll() {
        Element<T> nextElement = firstElement;
        System.out.print("Size = " + getSize() + ". Elements: ");
        while (nextElement != null) {
            System.out.print(nextElement.value + " ");
            nextElement = nextElement.next;
        }
        System.out.println();
    }

    public int getSize() {
        return size;
    }

    public void addR(T value) {
        Element<T> newElement = new Element<>(value);
        if (size == 0) {
            firstElement = newElement;
        } else {
            Element<T> nextElement = firstElement;
            while (nextElement.next != null) {
                nextElement = nextElement.next;
            }
            nextElement.next = newElement;
        }
        size++;
    }

    public void clearAll() {
        firstElement = null;
        size = 0;
    }

    public void removeLast() {
        if (size == 1) {
            firstElement = null;
            size--;
        } else {
            Element<T> nextElement = firstElement;
            while (nextElement.next.next != null) {
                nextElement = nextElement.next;
            }
            nextElement.next = null;
            size--;
        }

    }

    public boolean removeOfIndex(int index){
        if(size == 0 || index > size || index < 0){
            return false;
        }
        if(size == 1){
            clearAll();
            return true;
        }
        if(index == size){
            removeLast();
            return true;
        }
        if(index == 1){
            firstElement = firstElement.next;
            size--;
            return true;
        }
        int count =1;
        Element<T> nextElement = firstElement;
        Element<T> previousElement = firstElement;
        while (nextElement != null){
            count++;
            if(count == index){
                previousElement.next = nextElement.next.next;
                size--;
                return true;
            }
            previousElement = previousElement.next;
            nextElement = nextElement.next;
        }
        return false;
    }

    public boolean removeFirstOfValue(T value){
        if (size == 0) {
            return false;
        }else{
            Element<T> nextElement = firstElement;
            while(nextElement.next != null){
                if(nextElement.next.value != null &&  nextElement.next.value.equals(value)){
                    nextElement.next = nextElement.next.next;
                    size--;
                    return true;
                }
                nextElement = nextElement.next;
            }
        }
        return false;
    }

    public boolean removeAllOfValue(T value) {
        if(size == 0){
            return false;
        }
        boolean p = false;
        while(firstElement.next != null && firstElement.value.equals(value)){
            firstElement =firstElement.next;
            size--;
            p = true;
            if(firstElement == null) return true;
        }
        Element<T> nextElement = firstElement;
        while(nextElement != null){
            while (nextElement.next!=null && nextElement.next.value.equals(value)){
                nextElement.next = nextElement.next.next;
                size--;
                p = true;
            }
            if(nextElement.next == null) return true;
            nextElement = nextElement.next;
        }
        return p;
    }

    public void addOfIndex(int index, T value){
        if(index > 0 && index <= size+1) {
            Element<T> newElement = new Element<>(value);
            if(size == 0){
                firstElement = newElement;
                size++;
            }else if(index == 1){
                newElement.next = firstElement;
                firstElement = newElement;
                size++;
            }else {
                int count =1;
                Element<T> nextElement = firstElement;
                Element<T> previousElement = firstElement;
                while (nextElement != null){
                    count++;
                    if(count == index){
                        newElement.next = nextElement.next;
                        previousElement.next = newElement;
                        size++;
                        break;
                    }
                    previousElement = previousElement.next;
                    nextElement = nextElement.next;
                }
            }
        }
    }

}




