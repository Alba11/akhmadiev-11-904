package KFU.ITIS.s2.homeworks.hw2;

public interface ElementCollectionOption<T> {
    public void addR(T value);

    public void printAll();

    // Удаление первого вхождения занчения value
    public boolean removeFirstOfValue(T value);

    // Удаление элемента под номером index (начиная с 1)
    public boolean removeOfIndex(int index);

    // Удаление всех элементов со значением равным value
    public boolean removeAllOfValue(T value);

    // добавление элемента на позицию index
    public void addOfIndex(int index, T value);

    // длина списка
    public int getSize();

    //удаление последнего элемента в списке
    public void removeLast();

    // полная очистка списка
    public void clearAll();
}
