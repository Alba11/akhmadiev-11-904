package KFU.ITIS.s2.homeworks.hw2;

public class Main {
    public static void main(String[] args) {
        ElementCollection<String> elements = new ElementCollection<>();

        elements.printAll();
        System.out.println();

        System.out.println("1) RemoveFirstOfValue: ");
        elements.addMore("2", "1", "2", "1", "3");
        elements.printAll();
        elements.removeFirstOfValue("1");
        elements.printAll();
        elements.clearAll();
        System.out.println();

        System.out.println("2) RemoveOfIndex: ");
        elements.addMore("1", "2", "3", "4", "5", "6", "7");
        elements.printAll();
        elements.removeOfIndex(5);
        elements.printAll();
        elements.removeOfIndex(1);
        elements.printAll();
        elements.clearAll();
        System.out.println();

        System.out.println("3) RemoveAllOfValue: ");
        elements.addMore("1", "1", "1", "2", "3", "1", "1", "4", "1", "5", "1");
        elements.printAll();
        elements.removeAllOfValue("1");
        elements.printAll();
        elements.clearAll();
        System.out.println();

        System.out.println("4) addOfIndex: ");
        elements.addMore("1", "2", "3", "4", "5", "6", "7");
        elements.printAll();
        elements.addOfIndex(4, "9");
        elements.printAll();
        elements.addOfIndex(1, "10");
        elements.printAll();
        System.out.println();

        System.out.println("5) getSize: ");
        elements.printAll();
        System.out.println(elements.getSize());
        System.out.println();

        System.out.println("6) RemoveLast: ");
        elements.printAll();
        elements.removeLast();
        elements.printAll();
        System.out.println();

        System.out.println("7) ClearAll: ");
        elements.printAll();
        elements.clearAll();
        elements.printAll();
    }
}
