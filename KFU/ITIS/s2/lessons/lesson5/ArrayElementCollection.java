package KFU.ITIS.s2.lessons.lesson5;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;
import java.util.Spliterator;
import java.util.function.Consumer;

public class ArrayElementCollection<T> implements Iterable<T>, ArrayCollection<T> {
    private Object[] arr;
    private int emptySize;

    ArrayElementCollection(int lenght) {
        arr = new Object[lenght];
        emptySize = lenght;
    }

    public void add(T value) {
        if (emptySize > 0) {
            arr[arr.length - emptySize] = value;
            emptySize--;
        } else {
            emptySize = arr.length;
            arr = Arrays.copyOf(arr, arr.length * 2 + 1);
            arr[arr.length - emptySize - 1] = value;
        }
    }

    @Override
    public boolean add(int index, T value) {
        if(index >= 0 && index <= arr.length - emptySize) {
            arr[index] = value;
            return true;
        }
        return false;
    }

    public T get(int index) {
        return index >= 0 && index < arr.length - emptySize ?
                (T)arr[index] : null;
    }

    @Override
    public int indexOff(T value) {
        for(int i = 0; i < arr.length - emptySize; i++){
            if(arr[i].equals(value)) return i;
        }
        return -1;
    }

    @Override
    public int size() {
        return arr.length - emptySize;
    }

    @Override
    public T remove(int index) {
        if(index < 0 || index >= arr.length - emptySize){
            return null;
        }
        T a =(T) arr[index];
        if(index == arr.length){
            arr[index] = 0;
        }else {
            for (int i = index; i < arr.length - emptySize - 1; i++) {
                arr[i] = arr[i + 1];
            }
        }
        emptySize++;
        return a;
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayElementCollectionIterator<>();
    }

    private class ArrayElementCollectionIterator<T> implements Iterator<T> {
        int currentIndex = 0;
        @Override
        public boolean hasNext() {
            return currentIndex < arr.length-emptySize;
        }

        @Override
        public T next() {
            if(hasNext()){
                currentIndex++;
                return (T) arr[currentIndex-1];
            }
            return null;
        }
    }
}
