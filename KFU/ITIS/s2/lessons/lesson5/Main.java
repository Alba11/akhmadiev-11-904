package KFU.ITIS.s2.lessons.lesson5;

import javax.swing.text.html.HTMLDocument;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
        ArrayElementCollection<String> arrayElementCollection = new ArrayElementCollection<>(2);
        arrayElementCollection.add("One");
        arrayElementCollection.add("Two");
        arrayElementCollection.add("Tree");

        System.out.println(arrayElementCollection.get(0));
        System.out.println(arrayElementCollection.get(1));
        System.out.println(arrayElementCollection.get(2));
        for (String s : arrayElementCollection) {
            System.out.print(s + " ");
        }
        System.out.println();

        System.out.println(arrayElementCollection.size());

        System.out.println(arrayElementCollection.indexOff("Two"));

        System.out.println(arrayElementCollection.remove(2));

        for (String s : arrayElementCollection) {
            System.out.print(s + " ");
        }
    }
}
