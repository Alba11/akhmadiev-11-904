package KFU.ITIS.s2.tests.test1;

public interface Queue<E> extends Iterable<E>{
    boolean add(E e);

    boolean addAll(Iterable<E> iterable);

    int size();

    boolean isEmpty();

    E poll() throws NoElementExeption;

    E peek();
}
