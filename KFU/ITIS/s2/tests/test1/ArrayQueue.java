package KFU.ITIS.s2.tests.test1;

import java.util.Arrays;
import java.util.Iterator;
//1 var
public class ArrayQueue<T> implements Iterable<T>, Queue<T> {
    private Object[] arr;
    private int emptySize;

    ArrayQueue(int lenght) {
        arr = new Object[lenght];
        emptySize = lenght;
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayQueueIterator<>();
    }

    @Override
    public boolean add(T value) {
        if (emptySize > 0) {
            arr[arr.length - emptySize] = value;
            emptySize--;
        } else {
            emptySize = arr.length;
            arr = Arrays.copyOf(arr, arr.length * 2 + 1);
            arr[arr.length - emptySize - 1] = value;
        }
        return true;
    }

    @Override
    public boolean addAll(Iterable<T> iterable) {
        while (iterable.iterator().hasNext()){
            add(iterable.iterator().next());
        }
        return true;
    }

    @Override
    public int size() {
        return arr.length - emptySize;
    }

    @Override
    public boolean isEmpty() {
        return arr.length == emptySize;
    }

    @Override
    public T poll() throws NoElementExeption {
        if (isEmpty()) throw new NoElementExeption();
        T element = peek();
        arr[arr.length - emptySize - 1] = 0;
        emptySize++;
        return element;
    }

    @Override
    public T peek() {
        if (isEmpty()) return null;
        return (T) arr[arr.length - emptySize - 1];
    }

    private class ArrayQueueIterator<T> implements Iterator<T> {
        int currentIndex = 0;

        @Override
        public boolean hasNext() {
            return currentIndex < arr.length - emptySize;
        }

        @Override
        public T next() {
            if (hasNext()) {
                currentIndex++;
                return (T) arr[arr.length - emptySize - currentIndex];
            }
            return null;
        }
    }
}
