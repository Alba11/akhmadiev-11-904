package KFU.ITIS.s2.tests.test1;

import javax.swing.text.html.HTMLDocument;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) throws NoElementExeption {

        ArrayQueue<Integer> arrayQueue = new ArrayQueue<>(2);
        arrayQueue.add(1);
        arrayQueue.add(2);
        arrayQueue.add(3);
        arrayQueue.add(121);
        System.out.println("Head = " + arrayQueue.peek());
        System.out.println("Size = " + arrayQueue.size());

        for (Integer integer : arrayQueue) {
            System.out.print(integer + " ");
        }

        System.out.println();

        System.out.println("Head = " + arrayQueue.poll());

        System.out.println("Size = " + arrayQueue.size());

        for (Integer integer : arrayQueue) {
            System.out.print(integer + " ");
        }
        System.out.println();

        ArrayQueue<Integer> arrayQueueAdd = new ArrayQueue<>(3);
        arrayQueue.add(11);
        arrayQueue.add(12);
        arrayQueue.addAll(arrayQueueAdd);

        for (Integer integer : arrayQueue) {
            System.out.print(integer + " ");
        }

        System.out.println("Size = " + arrayQueue.size());
    }
}
