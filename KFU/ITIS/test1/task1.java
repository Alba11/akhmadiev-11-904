package ITIS.test1;
import java.util.Scanner;

public class task1{

    public static void main(String[] args){
	    Scanner sc = new Scanner(System.in);
	    
		double s = sc.nextInt();
		double k = s/60;
		if(k < 0 || k > 720){
			System.out.println("Error");
		}else {
			if(k >= 0.00 && k <= 4.59){
			System.out.println("Night");
			}else if(k <= 11.59){
			System.out.println("Morning");
			}else if(k <= 16.59){
			System.out.println("Day");
			}else {
			System.out.println("Afternoon");
			} 
  		
		}
	}	
} 