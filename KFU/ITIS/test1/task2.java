package ITIS.test1;
import java.util.Scanner;

public class task2{

    public static void main(String[] args){
	    Scanner sc = new Scanner(System.in);
	   
		int n = sc.nextInt();
        int x1 = 1;
		int x2 = n;
		if(n <= 0 || n%2 == 0){
			System.out.print("Error");
		}else{
			for(int i = 1; i <= n/2 + 1; i++){
				for(int j = n/2 + 1; j > i; j--){
					System.out.print(" ");
				}
				for(int j = 1; j <= x1; j++){
					System.out.print("*");
				}
				x1 += 2;
				System.out.println();
			}
			for(int i = 1; i <= n/2; i++){
				for(int j = 1; j <= i; j++){
					System.out.print(" ");
				}
				for(int j = 1; j <= n - 2 * i; j++){
					System.out.print("*");
				}
				System.out.println();
			}
		}
	}
} 